//
//  WelcomeViewController.swift
//  Tipatuity
//
//  Created by Victory on 14/06/16.
//  Copyright © 2016 elite. All rights reserved.
//

import UIKit
import Material


class WelcomeViewController: UIViewController, UIPageViewControllerDataSource, UIPageViewControllerDelegate {
    
    @IBOutlet weak var bottomView: UIView!
    
    @IBOutlet weak var btnNext: RaisedButton!
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblDescription: UILabel!
    
    var pageViewController: UIPageViewController!
    var contentImages: [String] = []
    
    var currentIndex = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view, typically from a nib.
        
        contentImages = ["welcome1_back", "welcome2_back", "welcome3_back", "welcome4_back"]
        
        initView()
    }
    
    func initView() {
        
        // create PageViewController
        createPageViewController()
        
        // customize UIPageControl
        setPageControl()
        
//        for name in UIFont.familyNames()
//        {
//            print(name)
//            print(UIFont.fontNamesForFamilyName(name))
//        }
    }
    
    // create page view controller
    private func createPageViewController() {
        
        pageViewController = self.storyboard?.instantiateViewControllerWithIdentifier("PageViewController") as! PageViewController
        pageViewController.dataSource = self
        pageViewController.delegate = self
        
        if (contentImages.count > 0) {
            
            let startingViewController = viewControllerAtIndex(currentIndex)!
            
            let contentViewControllers = [startingViewController]
            
            pageViewController.setViewControllers(contentViewControllers, direction: UIPageViewControllerNavigationDirection.Forward, animated: false, completion: nil)
        }
        
        // Change the size of page view controller
        pageViewController.view.frame = CGRectMake(0, 0, self.view.bounds.size.width, self.view.bounds.size.height * 380/600.0 + 2)
        
        // add PageViewController
        self.addChildViewController(pageViewController)
        
        self.view.addSubview(pageViewController!.view)
        pageViewController!.didMoveToParentViewController(self)
    }
    
    // customize page control
    private func setPageControl() {
        
        let appearance = UIPageControl.appearance()
        appearance.pageIndicatorTintColor = UIColor.lightGrayColor()
        appearance.currentPageIndicatorTintColor = UIColor.whiteColor()
        appearance.backgroundColor = UIColor.clearColor()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - UIPageViewControllerDataSource
    func pageViewController(pageViewController: UIPageViewController, viewControllerBeforeViewController viewController: UIViewController) -> UIViewController? {
        
        let contentViewController = viewController as! PageContentViewController
        
        if contentViewController.pageIndex > 0 {
            
            currentIndex = contentViewController.pageIndex - 1
            
            return viewControllerAtIndex(contentViewController.pageIndex-1)
        }
        
        return nil
    }
    
    func pageViewController(pageViewController: UIPageViewController, viewControllerAfterViewController viewController: UIViewController) -> UIViewController? {
        
        let contentViewController = viewController as! PageContentViewController
        
        if contentViewController.pageIndex+1 < contentImages.count {
            
            return viewControllerAtIndex(contentViewController.pageIndex+1)
        }
        
        return nil
    }
    
    // MARK: - UIPageViewControllerDelegate
    func pageViewController(pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
        
        
        let contentViewController = pageViewController.viewControllers?.last as! PageContentViewController
        
        currentIndex = contentViewController.pageIndex
        
        // change backgroundview
        
        let mediumFontAttribute = [NSForegroundColorAttributeName: UIColor.whiteColor(), NSFontAttributeName: UIFont(name: "Roboto-Medium", size: 15)!]
        var strDescription = ""
        let attributedText: NSMutableAttributedString!
        
        switch currentIndex {
            
        case 0:
            bottomView.backgroundColor = UIColor(netHex: 0x75cd00)
            lblTitle.text = "Welcome to Tipatuity"
            
            strDescription = "Tipatuity is the gratuity app you need! Whether you give tips or get them, Tipatuity helps you send money directly from your bank account to users around you right from your smartphone."
            
            attributedText = NSMutableAttributedString(string: strDescription)
            attributedText.addAttributes(mediumFontAttribute, range: NSMakeRange(0, 9))
            attributedText.addAttributes(mediumFontAttribute, range: NSMakeRange(75, 9))
            
            break
            
        case 1:
            bottomView.backgroundColor = UIColor(netHex: 0xff9800)
            lblTitle.text = "Gratitude is Attitude"
            
            strDescription = "You might have noticed that getting tips has become much harder. Why? Simple; we don't carry cash anymore. With Tipatuity, you can tip to practically anyone, anywhere, at any time."
            
            attributedText = NSMutableAttributedString(string: strDescription)
            attributedText.addAttributes(mediumFontAttribute, range: NSMakeRange(112, 9))
            
            break
            
        case 2:
            bottomView.backgroundColor = UIColor(netHex: 0xf90014)
            lblTitle.text = "Introducing Virtual Tip Jars"
            
            strDescription = "If you're working with a team, you can use Tipatuity to create a virtual tip jar and using our geo-location service, Customers can easily find you and leave a gratuity for the whole team!"
            
            attributedText = NSMutableAttributedString(string: strDescription)
            attributedText.addAttributes(mediumFontAttribute, range: NSMakeRange(43, 9))
            break
            
        default:
            bottomView.backgroundColor = UIColor(netHex: 0x75cd00)
            lblTitle.text = "Reserve your Username"
            
            strDescription = "All you need is your Tipatuity username. Print it on a card, a cup or even a napkin. Anything that will be noticed. Now that's easy!"
            
            attributedText = NSMutableAttributedString(string: strDescription)
            attributedText.addAttributes(mediumFontAttribute, range: NSMakeRange(21, 9))
            
            break
        }
        
        lblDescription.attributedText = attributedText
        
        if (currentIndex >= 3) {
            
            self.btnNext.setTitle("START USING TIPATUITY", forState: .Normal)
            
        } else {
            
            self.btnNext.setTitle("NEXT", forState: .Normal)
        }
    }
    
    // MARK: - Page Indicator
    func presentationCountForPageViewController(pageViewController: UIPageViewController) -> Int {
        
        return contentImages.count
    }
    
    func presentationIndexForPageViewController(pageViewController: UIPageViewController) -> Int {

        let mediumFontAttribute = [NSForegroundColorAttributeName: UIColor.whiteColor(), NSFontAttributeName: UIFont(name: "Roboto-Medium", size: 15)!]
        var strDescription = ""
        let attributedText: NSMutableAttributedString!
        
        switch currentIndex {
            
        case 0:
            
            self.bottomView.backgroundColor = UIColor(netHex: 0x75cd00)
            lblTitle.text = "Welcome to Tipatuity"
            
            strDescription = "Tipatuity is the gratuity app you need! Whether you give tips or get them, Tipatuity helps you send money directly from your bank account to users around you right from your smartphone."
            
            attributedText = NSMutableAttributedString(string: strDescription)
            attributedText.addAttributes(mediumFontAttribute, range: NSMakeRange(0, 9))
            attributedText.addAttributes(mediumFontAttribute, range: NSMakeRange(75, 9))
            
            break
            
        case 1:
            self.bottomView.backgroundColor = UIColor(netHex: 0xff9800)
            lblTitle.text = "Gratitude is Attitude"
            
            strDescription = "You might have noticed that getting tips has become much harder. Why? Simple; we don't carry cash anymore. With Tipatuity, you can tip to practically anyone, anywhere, at any time."
            
            attributedText = NSMutableAttributedString(string: strDescription)
            attributedText.addAttributes(mediumFontAttribute, range: NSMakeRange(112, 9))
            
            break
            
        case 2:
            self.bottomView.backgroundColor = UIColor(netHex: 0xf90014)
            lblTitle.text = "Introducing Virtual Tip Jars"
            
            strDescription = "If you're working with a team, you can use Tipatuity to create a virtual tip jar and using our geo-location service, Customers can easily find you and leave a gratuity for the whole team!"
            
            attributedText = NSMutableAttributedString(string: strDescription)
            attributedText.addAttributes(mediumFontAttribute, range: NSMakeRange(43, 9))
            break
            
        default:
            self.bottomView.backgroundColor = UIColor(netHex: 0x75cd00)
            lblTitle.text = "Reserve your Username"
            
            strDescription = "All you need is your Tipatuity username. Print it on a card, a cup or even a napkin. Anything that will be noticed. Now that's easy!"
            
            attributedText = NSMutableAttributedString(string: strDescription)
            attributedText.addAttributes(mediumFontAttribute, range: NSMakeRange(21, 9))
            
            break
        }
        
        lblDescription.attributedText = attributedText
        
        return currentIndex
    }
    
    private func viewControllerAtIndex(index: Int) -> PageContentViewController? {
        
        if (index < contentImages.count) {
            
            let contentViewController = self.storyboard?.instantiateViewControllerWithIdentifier("PageContentViewController") as! PageContentViewController
            
            contentViewController.pageIndex = index
            contentViewController.imageName = contentImages[index]
            
            return contentViewController
        }
        
        return nil
    }
    
    // MARK: - IBAction
    @IBAction func nextTapped(sender: UIButton) {
        
        // for test
//        let contentViewControllers = pageViewController.viewControllers
//        print(contentViewControllers?.count)
        
        if (currentIndex < 3) {
            
            currentIndex += 1
            
            // show next
            pageViewController.setViewControllers([viewControllerAtIndex(currentIndex)!], direction: .Forward, animated: true, completion: nil)
            
            if (currentIndex >= 3) {
                
                btnNext.setTitle("START USING TIPATUITY", forState: .Normal)
                
            } else {
                
                btnNext.setTitle("NEXT", forState: .Normal)
            }
            
        } else {
            
            // go to credential view
            UIApplication.sharedApplication().keyWindow?.rootViewController = self.storyboard?.instantiateViewControllerWithIdentifier("CredentialNav") as! UINavigationController
        }
    }
}

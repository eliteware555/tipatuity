//
//  ContentViewController.swift
//  Tipatuity
//
//  Created by Victory on 14/06/16.
//  Copyright © 2016 elite. All rights reserved.
//

import UIKit

class PageContentViewController: UIViewController {
    
    @IBOutlet weak var contentImageView: UIImageView!
    
    var backgroundColor: UIColor = UIColor.whiteColor()
    
    var imageName: String = "" {
        
        didSet {
        
            
            if let imageView = contentImageView {
                
                imageView.image = UIImage(named: imageName)
                
            }
        }
    }
    var pageIndex: Int = 0
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        initView()
    }
    
    func initView() {
        
        // change background color
        
        contentImageView.image = UIImage(named: imageName)
        contentImageView.contentMode = .ScaleAspectFill
        contentImageView.clipsToBounds = true
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}

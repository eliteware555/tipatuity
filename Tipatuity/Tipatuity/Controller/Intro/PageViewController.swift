//
//  WelcomeViewController.swift
//  Tipatuity
//
//  Created by Victory on 13/06/16.
//  Copyright © 2016 elite. All rights reserved.
//

import UIKit

class PageViewController: UIPageViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidLayoutSubviews() {
        
        super.viewDidLayoutSubviews()
        
        let subViews: NSArray = view.subviews
        var scrollView: UIScrollView? = nil
        var pageControl: UIPageControl? = nil
        
        for view in subViews {
            
            if view.isKindOfClass(UIScrollView) {
                
                scrollView = view as? UIScrollView
                
            } else if view.isKindOfClass(UIPageControl) {
                
                pageControl = view as? UIPageControl
            }
        }
        
        if (scrollView != nil && pageControl != nil) {
            
            scrollView?.frame = view.bounds
            view.bringSubviewToFront(pageControl!)
        }
    } 
}

//
//  BaseTableViewController.swift
//  IntShop
//
//  Created by Victory on 13/06/16.
//  Copyright © 2016 victory. All rights reserved.
//

import UIKit
import Material

class BaseTableViewController: UITableViewController {
    
    var progressView: MBProgressHUD?
    
    var loadingView: LoadingView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        
        self.view.endEditing(true)
    }
    
    // MARK: - UITableViewDelegate
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        self.view.endEditing(true)
    }
    
    // register keyboard notification
    func registerForKeyboardNotifications() {
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(keyboardWillShow(_:)), name: UIKeyboardWillShowNotification, object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(keyboardWillHide(_:)), name: UIKeyboardWillHideNotification, object: nil)
    }
    
    // remove keyboard notification
    func deregisrerForKeyboardNotifications() {
        
        NSNotificationCenter.defaultCenter().removeObserver(UIKeyboardWillShowNotification)
        NSNotificationCenter.defaultCenter().removeObserver(UIKeyboardWillHideNotification)
    }
    
    // when keyboard is showing, move view up
    func keyboardWillShow(notification: NSNotification) {
        
        let userInfo:NSDictionary = notification.userInfo!
        let keyboardFrame:NSValue = userInfo.valueForKey(UIKeyboardFrameEndUserInfoKey) as! NSValue
        let keyboardRectangle = keyboardFrame.CGRectValue()
        
        let offsetY = keyboardRectangle.height - getOffsetYWhenShowKeyboard()
        
        UIView.animateWithDuration(0.2) { () -> Void in
            
            var f = self.view.frame
            
            f.origin.y = -offsetY;
            
            self.view.frame = f;
        }
    }
    
    // when keyboard is closing, move view down
    func keyboardWillHide(notification: NSNotification) {
        
        UIView.animateWithDuration(0.2) { () -> Void in
            
            var f = self.view.frame
            
            f.origin.y = 0
            
            self.view.frame = f;
        }
    }
    
    // get offset
    func getOffsetYWhenShowKeyboard() -> CGFloat {
        
        return 0
    }

    // show loading view
    func showLoadingViewWithTitle(title: String, buttonTitle: String) {
        
        if (loadingView == nil) {
            
            loadingView = LoadingView(frame: CGRectMake(0, 0, self.view.bounds.size.width, self.view.bounds.size.height))
            
            loadingView.cancelDelegate = self
        }
        
        loadingView.showLoadingWithTitle(title, buttonTitle: buttonTitle, onView: self.view)
    }
    
    func hideLoadingView() {
        
        loadingView.hideLoadingView()
        loadingView = nil
    }
    
    // show loading view with title
    func showHUDLoadingViewWithTitle(title: String!, sender: AnyObject) {
        
        if (progressView == nil) {
            
            progressView = MBProgressHUD(view: self.view)
        }
        
        self.view.addSubview(progressView!)
        
        progressView?.color = UIColor(red: 255/255.0, green: 87/255.0, blue: 34/255.0, alpha: 0.75)
        
        progressView!.labelText = title
        progressView?.show(true)
    }
    
    // hide loading view
    func hideHUDLoadingView() {
        
        if (progressView != nil) {
            
            progressView?.hide(true)
            
            progressView = nil
        }
    }
    
    // hide loading view after delay
    func hideHUDLoadingView(delay: NSTimeInterval!) {
        
        progressView?.hide(true, afterDelay: delay)
    }
}

extension BaseTableViewController: LoadingViewDelegate {
    
    func cancelButtonClicked(sender: LoadingView) {
        
        sender.hideLoadingView()
        
        loadingView = nil
    }
}






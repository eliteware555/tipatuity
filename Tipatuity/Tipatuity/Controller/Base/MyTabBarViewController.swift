//
//  MyTapBarViewController.swift
//  Tipatuity
//
//  Created by Victory on 14/06/16.
//  Copyright © 2016 elite. All rights reserved.
//

import UIKit

class MyTabBarViewController: BFPaperTabBarController, UITabBarControllerDelegate {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        setupTabBar()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Navigation
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        
        // Get the new view controller using segue.destinationViewController
        // Pass the selected object to the new view controller
    }
    
    func setupTabBar() {
        
        // set the tab bar tint color to something cool
        self.tabBar.tintColor = UIColor.whiteColor()
        
        self.delegate = self    // Just to demo that delegate methods are being called
        
        // Set this to adjust the thickness (height) of the underline bar. Not that any value greater than 1 could cover up parts of the TabBarItem's title
        self.underlineThickness = 4.0
    }

    // MARK: - UITabBarController Delegate
    func tabBarController(tabBarController: UITabBarController, shouldSelectViewController viewController: UIViewController) -> Bool {
        
//        print("UITabBarDelegate: shouldSelectViewController...")
        
        return true
    }
    
    func tabBarController(tabBarController: UITabBarController, didSelectViewController viewController: UIViewController) {
        
//        print("UITabBarDelegate: didSelectViewController...")
    }
    
    func tabBarController(tabBarController: UITabBarController, willBeginCustomizingViewControllers viewControllers: [UIViewController]) {
        
//        print("UITabBarDelegate: willBeginCustomizingViewControllers...")
    }
    
    func tabBarController(tabBarController: UITabBarController, didEndCustomizingViewControllers viewControllers: [UIViewController], changed: Bool) {
        
//        print("UITabBarDelegate: didEndCustomizingViewControllers...")
    }
}


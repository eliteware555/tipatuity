//
//  BaseViewController.swift
//  IntShop
//
//  Created by Victory on 12/06/16.
//  Copyright © 2016 victory. All rights reserved.
//

import UIKit
import Material

class BaseViewController: UIViewController {
    
    var progressView: MBProgressHUD?
    
    var loadingView: LoadingView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        
        self.view.endEditing(true)
    }
    
    // register keyboard notification
    func registerForKeyboardNotifications() {
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(keyboardWillShow(_:)), name: UIKeyboardWillShowNotification, object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(keyboardWillHide(_:)), name: UIKeyboardWillHideNotification, object: nil)
    }
    
    // remove keyboard notification
    func deregisrerForKeyboardNotifications() {
        
        NSNotificationCenter.defaultCenter().removeObserver(UIKeyboardWillShowNotification)
        NSNotificationCenter.defaultCenter().removeObserver(UIKeyboardWillHideNotification)
    }
    
    // when keyboard is showing, move view up
    func keyboardWillShow(notification: NSNotification) {
        
        let userInfo:NSDictionary = notification.userInfo!
        let keyboardFrame:NSValue = userInfo.valueForKey(UIKeyboardFrameEndUserInfoKey) as! NSValue
        let keyboardRectangle = keyboardFrame.CGRectValue()

        let offsetY = keyboardRectangle.height - getOffsetYWhenShowKeyboard()
        
        UIView.animateWithDuration(0.2) { () -> Void in
            
            var f = self.view.frame
            
            f.origin.y = -offsetY;
            
            self.view.frame = f;
        }
    }
    
    // when keyboard is closing, move view down
    func keyboardWillHide(notification: NSNotification) {
        
        UIView.animateWithDuration(0.2) { () -> Void in
            
            var f = self.view.frame
            
            if ((self.navigationController?.navigationBar) != nil) {
            
                f.origin.y = 64
                
            } else {
                
                f.origin.y = 0
            }
            
            self.view.frame = f;
        }
    }
    
    // get offset
    func getOffsetYWhenShowKeyboard() -> CGFloat {
        
        return 0
    }
    
    // show loading view
    func showLoadingViewWithTitle(title: String, buttonTitle: String) {
        
        if (loadingView == nil) {
            
            loadingView = LoadingView(frame: CGRectMake(0, 0, self.view.bounds.size.width, self.view.bounds.size.height))
            
            loadingView.cancelDelegate = self
        }
        
        loadingView.showLoadingWithTitle(title, buttonTitle: buttonTitle, onView: self.view)
    }
    
    // hide image loading view
    func hideLoadingView() {
        
        if (loadingView != nil) {
        
            loadingView.hideLoadingView()
            loadingView = nil
        }
    }
    
    // show loading view with title
    func showHUDLoadingViewWithTitle(title: String!, sender: AnyObject) {
        
        if (progressView == nil) {
            
            progressView = MBProgressHUD(view: self.view)
        }
        
        self.view.addSubview(progressView!)
        
        progressView?.color = UIColor(hex: 0x00BBFF)
        
        progressView!.labelText = title
        progressView?.show(true)
    }
    
    // hide loading view
    func hideHUDLoadingView() {
        
        if (progressView != nil) {
            
            progressView?.hide(true)
            
            progressView = nil
        }
    }
    
    // hide loading view after delay
    func hideHUDLoadingView(delay: NSTimeInterval!) {
        
        progressView?.hide(true, afterDelay: delay)
    }
    
    func showAlertDialog(title: String!, message: String!, positive: String!, negative: String!) {
        
        let alert = UIAlertController(title: title, message: message, preferredStyle: .Alert)
        
        if (positive != nil) {
            
            alert.addAction(UIAlertAction(title: positive, style: .Default, handler: nil))
        }
        
        if (negative != nil) {
            
            alert.addAction(UIAlertAction(title: positive, style: .Default, handler: nil))
        }
        
        self.presentViewController(alert, animated: true, completion: nil)
        alert.view.tintColor = UIColor(netHex: 0x00BBFF)
    }
}

extension BaseViewController: LoadingViewDelegate {

    func cancelButtonClicked(sender: LoadingView) {
        
        sender.hideLoadingView()
        
        loadingView = nil
    }
}





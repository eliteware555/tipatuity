//
//  SecurityViewController.swift
//  Tipatuity
//
//  Created by Victory on 21/06/16.
//  Copyright © 2016 elite. All rights reserved.
//

import UIKit
import JLToast
import Material

class SecurityViewController: BaseTableViewController, UITextFieldDelegate, CustomActionSheetDelegate {
    
    @IBOutlet weak var userNameField: UITextField!
    @IBOutlet weak var passwordField:UITextField!
    
    @IBOutlet weak var usernameInfoView: UIView!

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        initView()
    }
    
    func initView() {
        
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .Plain, target: nil, action: nil)
        
        // disable input of username field and password field
        userNameField.userInteractionEnabled = false
        passwordField.userInteractionEnabled = false
        
        usernameInfoView.hidden = true
        usernameInfoView.alpha = 0.0
    }
    
    override func viewWillAppear(animated: Bool) {
        
        super.viewWillAppear(animated)
        
        registerForKeyboardNotifications()
    }
    
    override func getOffsetYWhenShowKeyboard() -> CGFloat {
        
        return 80
    }
    
    override func viewWillDisappear(animated: Bool) {
        
        super.viewWillDisappear(animated)
        
        deregisrerForKeyboardNotifications()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func enablePinCodeSwitched(sender: MaterialSwitch) {

        if (sender.on) {
 
           self.performSegueWithIdentifier("SegueSecurity2EnterPinCode", sender: self)
        }
    }
    
    @IBAction func passwordEditTapped(sender: AnyObject) {
        
//        passwordField.userInteractionEnabled = true
//        passwordField.becomeFirstResponder()
        
        let destVC = self.storyboard?.instantiateViewControllerWithIdentifier("ForgotPasswordViewController") as! ForgotPasswordViewController
        
        destVC.modalTransitionStyle = .CoverVertical
        destVC.modalPresentationStyle = .OverFullScreen
        
        self.presentViewController(destVC, animated: true, completion: nil)
    }
    
    // MARK: - UITextField delegate
    func textFieldDidEndEditing(textField: UITextField) {
        
        textField.userInteractionEnabled = false
    }
    
    func hideUsernameInfoView() {
        
        UIView.animateWithDuration(0.5, animations: { () -> Void in
            
            self.usernameInfoView.alpha = 0
            
            }, completion: { finished in
                
                self.usernameInfoView.hidden = finished
        })
    }
    
    @IBAction func usernameInfoTapped(sender: AnyObject) {
        
        usernameInfoView.hidden = false
        
        UIView.animateWithDuration(0.5, animations: {  () -> Void in
            
            self.usernameInfoView.alpha = 1
            
            }, completion: { finished in
                
                self.performSelector(#selector(self.hideUsernameInfoView), withObject: nil, afterDelay: 1.5)
        })
    }
    
    @IBAction func settingMenuTapped(ender: AnyObject) {
        
        let actionSheet = CustomActionSheet(delegate: self, buttonTitles: ["Invite Friends", "Settings", "Support", "Cancel"], images: ["ic_share", "ic_setting", "ic_support", "ic_cancel"], startIndex: 0)
        actionSheet.showAlert()
    }
    
    func modalAlertPressed(alert: CustomActionSheet, withButtonIndex: Int) {
        
        //        print("button pressed - \(withButtonIndex)")
        
        switch withButtonIndex {
            
        case 0:
            
            JLToast.makeText("Invite Freidns Pressed", duration: 1.5).show()
            break
            
        case 1:
            
            let settingVC = self.storyboard?.instantiateViewControllerWithIdentifier("SettingNav") as! UINavigationController
            
            self.presentViewController(settingVC, animated: true, completion: nil)
            
            break
            
        case 2:
            
            JLToast.makeText("Support Pressed", duration: 1.5).show()
            break
            
        default:
            break
        }
    }
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        
        
        if  segue.identifier == "SegueSecurity2ChangePincode" {
            
            let destVC = (segue.destinationViewController as! UINavigationController).topViewController as! PinCodeViewController
            destVC.pincodeType = PinCodeType.CHANGE_CODE
            
        } else if (segue.identifier == "SegueSecurity2EnterPinCode") {
            
            let destVC = (segue.destinationViewController as! UINavigationController).topViewController as! PinCodeViewController
            destVC.pincodeType = PinCodeType.CHANGE_CODE
        }
    }
}

//
//  ProfileViewController.swift
//  Tipatuity
//
//  Created by Victory on 20/06/16.
//  Copyright © 2016 elite. All rights reserved.
//

import UIKit
import JLToast

class ProfileViewController: BaseTableViewController, UITextFieldDelegate, UITextViewDelegate, UINavigationControllerDelegate, UIImagePickerControllerDelegate, CustomActionSheetDelegate {
    
    @IBOutlet weak var userProfileView: UIImageView!
    
    @IBOutlet weak var displayNameField: UITextField!
    @IBOutlet weak var occupationField: UITextField!
    @IBOutlet weak var bioField: UITextView!
    
    // saved photo path
    var strPhotoPath: String = ""
    
    // UIImagePickerController
    var picker: UIImagePickerController? = UIImagePickerController()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        initView()
    }
    
    func initView() {
       
        // customize back bar button item
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .Plain, target: nil, action: nil)
        navigationController?.navigationBar.tintColor = UIColor.whiteColor()
        
        displayNameField.userInteractionEnabled = false
        occupationField.userInteractionEnabled = false
        bioField.userInteractionEnabled = false
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - UIImagePickerControllerDelgeate
    // open gallery
    func openGallery() {
        
        picker?.sourceType = UIImagePickerControllerSourceType.PhotoLibrary
        picker?.allowsEditing = true
        picker?.delegate = self
        self.presentViewController(picker!, animated: true, completion: nil)
    }
    
    // open camera
    func openCamera() {
        
        if(UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.Camera)) {
            
            picker?.sourceType = UIImagePickerControllerSourceType.Camera
            picker?.allowsEditing = true
            picker?.modalPresentationStyle = .FullScreen
            picker?.delegate = self
            
            self.presentViewController(picker!, animated: true, completion: nil)
        }
    }
    
    // MARK: - UIImagePickerController Delegate
    func imagePickerControllerDidCancel(picker: UIImagePickerController) {
        
        dismissViewControllerAnimated(true, completion: nil)
    }
    
    func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : AnyObject]) {
        
        if let newImage = info["UIImagePickerControllerEditedImage"] as? UIImage {
            
            strPhotoPath = CommonUtils.saveToFile(newImage)
            
            userProfileView.contentMode = .ScaleAspectFit
            userProfileView.image = UIImage(contentsOfFile: strPhotoPath)
            
        }
        
        picker.dismissViewControllerAnimated(true, completion: nil)
    }
    
    // MARK - UITexTField Delegate
    func textFieldDidEndEditing(textField: UITextField) {
        
        textField.userInteractionEnabled = false
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        
        textField.resignFirstResponder()
        
        textField.userInteractionEnabled = false
        
        return true
    }
    
    // MARK - UITextView delegate
    func textViewDidEndEditing(textView: UITextView) {
        
        textView.userInteractionEnabled = false
    }

    @IBAction func facebookLinkSwitched(sender: AnyObject) {
        
    }
    
    @IBAction func twitterLinkSwitched(sender: AnyObject) {
        
    }
    
    @IBAction func linkedInLinkSwitched(sender: AnyObject) {
        
    }
    
    @IBAction func profileEditTapped(sender: UIButton) {
        
        switch sender.tag - 310 {
        case 0:
            // edit profile image
            
            let alert = UIAlertController(title: nil, message: nil, preferredStyle: .ActionSheet)
            let cameraAction = UIAlertAction(title: "Camera", style: .Default) { (alert: UIAlertAction) -> Void in
                self.openCamera()
            }
            let albumAction = UIAlertAction(title: "Gallery", style: .Default) { (alert: UIAlertAction!) -> Void in
                self.openGallery()
            }
            let cancelAction = UIAlertAction(title: "Cancel", style: .Cancel) { (alert: UIAlertAction!) -> Void in
                
            }
            
            picker?.delegate = self
            
            alert.addAction(cameraAction)
            alert.addAction(albumAction)
            alert.addAction(cancelAction)
            presentViewController(alert, animated: true, completion: nil)
            
            break
            
        case 1:
            // edit display name
            displayNameField.userInteractionEnabled = true
            displayNameField.becomeFirstResponder()
            
            break
            
        case 2:
            // edit occupation
            occupationField.userInteractionEnabled = true
            occupationField.becomeFirstResponder()
            break
            
        default:
            // edit bio
            bioField.userInteractionEnabled = true
            bioField.becomeFirstResponder()
            break
        }
    }
    
    @IBAction func settingMenuTapped(ender: AnyObject) {
        
        let actionSheet = CustomActionSheet(delegate: self, buttonTitles: ["Invite Friends", "Settings", "Support", "Cancel"], images: ["ic_share", "ic_setting", "ic_support", "ic_cancel"], startIndex: 0)
        actionSheet.showAlert()
    }
    
    func modalAlertPressed(alert: CustomActionSheet, withButtonIndex: Int) {
        
        //        print("button pressed - \(withButtonIndex)")
        
        switch withButtonIndex {
            
        case 0:
            
            JLToast.makeText("Invite Freidns Pressed", duration: 1.5).show()
            break
            
        case 1:
            
            let settingVC = self.storyboard?.instantiateViewControllerWithIdentifier("SettingNav") as! UINavigationController
            
            self.presentViewController(settingVC, animated: true, completion: nil)
            
            break
            
        case 2:
            
            JLToast.makeText("Support Pressed", duration: 1.5).show()
            break
            
        default:
            break
        }
    }
    
    // MARK: - UITableViewDelegate
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        super.tableView(tableView, didSelectRowAtIndexPath: indexPath)
        
        if (indexPath.row == 3) {
            
            JLToast.makeText("Account Status pressed", duration: 1.0).show()
        }
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        
        var strBackTitle = ""
        
        if (segue.identifier == "SegueProfile2BankAccount") {
            
            strBackTitle = "Bank Account"
            
        } else {
            
        }
        
        navigationItem.backBarButtonItem = UIBarButtonItem(title: strBackTitle, style: .Plain, target: nil, action: nil)
    }

}






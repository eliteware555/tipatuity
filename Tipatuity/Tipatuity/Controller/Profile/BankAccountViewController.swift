//
//  BankAccountViewController.swift
//  Tipatuity
//
//  Created by Victory on 21/06/16.
//  Copyright © 2016 elite. All rights reserved.
//

import UIKit
import JLToast

class BankAccountViewController: BaseViewController, UITableViewDataSource, UITableViewDelegate, CustomActionSheetDelegate {
    
    @IBOutlet weak var tblBank: UITableView!

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        initView()
    }
    
    func initView() {
        
        // change status bar style ( white color)
        self.navigationController!.navigationBar.barStyle = .BlackTranslucent;
        
        self.navigationController?.navigationBar.translucent = false
        
        // remove navigation bar 1px bottom line
        let image = UIImage()
        self.navigationController?.navigationBar.shadowImage = image
        self.navigationController?.navigationBar.setBackgroundImage(image, forBarMetrics: .Default)        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - UITableViewDataSource
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return 3
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCellWithIdentifier("BankAccountCell") as! BankAccountCell
        
        switch indexPath.row {
        case 0:
            cell.lblBankName.text = "AB Bank - 0354"
            cell.lblStatus.text = "Verified"
            cell.lblStatus.textColor = UIColor(hex: 0x7ed321)
            break
            
        case 1:
            
            cell.lblBankName.text = "FIRST SECURITY BANK - 7867"
            cell.lblStatus.text = "Pending"
            cell.lblStatus.textColor = UIColor(hex: 0xF5A623)
            
            break
            
        default:
            
            cell.lblBankName.text = "FIRST SECUTIRY BANK - 7867"
            cell.lblStatus.text = "Unverified"
            cell.lblStatus.textColor = UIColor(hex: 0xE20936)
            
            break
        }
        
        return cell
    }
    
    // MARK: - IBAction
    @IBAction func bankFlatTapped(sender: AnyObject) {
        
    }
    
    @IBAction func settingMenuTapped(ender: AnyObject) {
        
        let actionSheet = CustomActionSheet(delegate: self, buttonTitles: ["Invite Friends", "Settings", "Support", "Cancel"], images: ["ic_share", "ic_setting", "ic_support", "ic_cancel"], startIndex: 0)
        actionSheet.showAlert()
    }
    
    func modalAlertPressed(alert: CustomActionSheet, withButtonIndex: Int) {
        
        //        print("button pressed - \(withButtonIndex)")
        
        switch withButtonIndex {
            
        case 0:
            
            JLToast.makeText("Invite Freidns Pressed", duration: 1.5).show()
            break
            
        case 1:
            
            let settingVC = self.storyboard?.instantiateViewControllerWithIdentifier("SettingNav") as! UINavigationController
            
            self.presentViewController(settingVC, animated: true, completion: nil)
            
            break
            
        case 2:
            
            JLToast.makeText("Support Pressed", duration: 1.5).show()
            break
            
        default:
            break
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

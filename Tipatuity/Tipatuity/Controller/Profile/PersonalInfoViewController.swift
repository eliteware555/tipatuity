//
//  PersonalInfoViewController.swift
//  Tipatuity
//
//  Created by Victory on 21/06/16.
//  Copyright © 2016 elite. All rights reserved.
//

import UIKit
import Material
import FontAwesome
import JLToast

class PersonalInfoViewController: BaseTableViewController, UITextFieldDelegate, CustomActionSheetDelegate {
    
    @IBOutlet weak var firstNameField: UITextField!
    @IBOutlet weak var familyNameField: UITextField!
    
    @IBOutlet weak var firstNameInfoView: UIView!
    @IBOutlet weak var familyNameInfoView: UIView!
    
    @IBOutlet weak var dateField: UITextField!
    @IBOutlet weak var monthField: UITextField!
    @IBOutlet weak var yearField: UITextField!
    
    @IBOutlet weak var emailField: UITextField!
    @IBOutlet weak var phoneField: UITextField!
    
    @IBOutlet weak var address1Field: UITextField!
    @IBOutlet weak var address2Field: UITextField!
    
    @IBOutlet weak var cityField: UITextField!
    @IBOutlet weak var zipCodeField: UITextField!
    @IBOutlet weak var btnState: UIButton!
    
    let dropDown = DropDown()
    
    override func viewDidLoad() {
        
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        initView()
    }
    
    func initView() {
        
//        let darkGrayAttribute = [NSForegroundColorAttributeName: UIColor.darkGrayColor(), NSFontAttributeName: UIFont.systemFontOfSize(17)]
//        
//        let strFirstName = "Asif "
//        
//        firstNameField.font = UIFont.fontAwesomeOfSize(14)
//        let attributedFirstName = NSMutableAttributedString(string: strFirstName + String.fontAwesomeIconWithCode("fa-lock")!)
//        attributedFirstName.addAttributes(darkGrayAttribute, range: NSMakeRange(strFirstName.characters.count, 1))
//        firstNameField.attributedText = attributedFirstName
//        
//        familyNameField.font = UIFont.fontAwesomeOfSize(14)
//        let strFamilyName = "Chowdgruy "
//        let attributedFamilyName = NSMutableAttributedString(string: strFamilyName + String.fontAwesomeIconWithCode("fa-lock")!)
//        attributedFamilyName.addAttributes(darkGrayAttribute, range: NSMakeRange(strFamilyName.characters.count, 1))
//        familyNameField.attributedText = attributedFamilyName
        
        firstNameField.font = UIFont.fontAwesomeOfSize(17)
        firstNameField.text = "Asif " + String.fontAwesomeIconWithCode("fa-lock")!
        
        familyNameField.font = UIFont.fontAwesomeOfSize(17)
        familyNameField.text = "Chowdhury " + String.fontAwesomeIconWithCode("fa-lock")!
        
        firstNameInfoView.hidden = true
        firstNameInfoView.alpha = 0
        
        familyNameInfoView.hidden = true
        familyNameInfoView.alpha = 0
        
        // date of birthday input with date picker
        
        dateField.delegate = self
        monthField.delegate = self
        yearField.delegate = self
        
        dateField.tintColor = UIColor.clearColor()
        monthField.tintColor = UIColor.clearColor()
        yearField.tintColor = UIColor.clearColor()
        
        // disable input for phone field & address field
        phoneField.userInteractionEnabled = false
        
        emailField.userInteractionEnabled = false
        
        address1Field.userInteractionEnabled = false
        address2Field.userInteractionEnabled = false
        cityField.userInteractionEnabled = false
        zipCodeField.userInteractionEnabled = false
        btnState.userInteractionEnabled = false
        
        phoneField.delegate = self
        emailField.delegate = self
        
        address1Field.delegate = self
        address2Field.delegate = self
        cityField.delegate = self
        zipCodeField.delegate = self
        
        dropDown.dataSource = CommonUtils.getStateList()
        
        dropDown.selectionAction = { [unowned self] (index, item) in self.btnState.setTitle(item, forState: .Normal)
        }
        
        dropDown.anchorView = btnState
        dropDown.bottomOffset = CGPoint(x: 0, y: self.btnState.frame.origin.y)
        dropDown.topOffset = CGPoint(x: 0, y: self.btnState.frame.origin.y)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // UITextFieldDelegate
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        
        if (textField == dateField || textField == monthField || textField == yearField) {
            
            return false
            
        } else {
            
            return true
        }
    }
    
    func textFieldDidBeginEditing(textField: UITextField) {
        
        if (textField == dateField || textField == monthField || textField == yearField) {
            
            let datePickerView: UIDatePicker = UIDatePicker()
            datePickerView.datePickerMode = UIDatePickerMode.Date
            
            textField.inputView = datePickerView
            datePickerView.addTarget(self, action: #selector(datePickerValueChanged(_:)), forControlEvents: UIControlEvents.ValueChanged)
        }
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        
        if (textField == firstNameField) {
        
            familyNameField.becomeFirstResponder()
            
        } else if (textField == address1Field) {
            
            address2Field.becomeFirstResponder()
            
        } else if (textField == address2Field) {
            
            cityField.becomeFirstResponder()
            
        } else if (textField == cityField) {
            
            zipCodeField.becomeFirstResponder()
        }
        
        textField.resignFirstResponder()
        
        return true
    }
    
    func datePickerValueChanged(sender: UIDatePicker) {
        
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "dd-MM-yyyy"
        
        let date = sender.date
        
        let calendar = NSCalendar.currentCalendar()
        let components = calendar.components([.Day , .Month , .Year], fromDate: date)
        
        dateField.text = "\(components.day)"
        monthField.text = "\(components.month)"
        yearField.text = "\(components.year)"
    }
    
    func textFieldDidEndEditing(textField: UITextField) {
        
        if (textField == emailField || textField == phoneField) {
            
            textField.userInteractionEnabled = false
            
        } else if (textField == zipCodeField) {
            
            address1Field.userInteractionEnabled = false
            address2Field.userInteractionEnabled = false
            cityField.userInteractionEnabled = false
            zipCodeField.userInteractionEnabled = false
            btnState.userInteractionEnabled = false
        }
    }
    
    func showInfoView(index: Int) {
        
        if (index == 0) {
            
            firstNameInfoView.hidden = false
            
            UIView.animateWithDuration(0.5, animations: {  () -> Void in
                
                self.firstNameInfoView.alpha = 1
                
                }, completion: { finished in
                    
                    self.performSelector(#selector(self.hideFirstNameInfoView), withObject: nil, afterDelay: 1.5)
            })
            
        } else {
            
            familyNameInfoView.hidden = false
            
            UIView.animateWithDuration(0.5, animations: {  () -> Void in
                
                self.familyNameInfoView.alpha = 1
                
                }, completion: { finished in
                    
                    self.performSelector(#selector(self.hideMailNameInfoView), withObject: nil, afterDelay: 1.5)
            })
        }
    }
    
    func hideFirstNameInfoView() {
        
        UIView.animateWithDuration(0.5, animations: { () -> Void in
            
            self.firstNameInfoView.alpha = 0
            
            }, completion: { finished in
                
                self.firstNameInfoView.hidden = finished
        })
    }
    
    func hideMailNameInfoView() {
        
        UIView.animateWithDuration(0.5, animations: { () -> Void in
            
            self.familyNameInfoView.alpha = 0
            
            }, completion: { finished in
                
                self.familyNameInfoView.hidden = finished
        })
    }
    
    @IBAction func settingMenuTapped(sender: AnyObject) {
        
        self.view.endEditing(true)
        
        let actionSheet = CustomActionSheet(delegate: self, buttonTitles: ["Invite Friends", "Settings", "Support", "Cancel"], images: ["ic_share", "ic_setting", "ic_support", "ic_cancel"], startIndex: 0)
        actionSheet.showAlert()
    }
    
    func modalAlertPressed(alert: CustomActionSheet, withButtonIndex: Int) {
        
        //        print("button pressed - \(withButtonIndex)")
        
        switch withButtonIndex {
            
        case 0:
            
            JLToast.makeText("Invite Freidns Pressed", duration: 1.5).show()
            break
            
        case 1:
            
            let settingVC = self.storyboard?.instantiateViewControllerWithIdentifier("SettingNav") as! UINavigationController
            
            self.presentViewController(settingVC, animated: true, completion: nil)
            
            break
            
        case 2:
            
            JLToast.makeText("Support Pressed", duration: 1.5).show()
            break
            
        default:
            break
        }
    }
    
    @IBAction func firstNameInfoTapped(sender: UIButton) {
        
        showInfoView(0)
    }
    
    @IBAction func lastNameInfoTapped(sender: UIButton) {
        
        showInfoView(1)
    }
    
    @IBAction func emailEditTapped(sender: AnyObject) {
        
        emailField.userInteractionEnabled = true
        emailField.becomeFirstResponder()
    }
    
    @IBAction func phoneEditTapped(sender: AnyObject) {
        
        phoneField.userInteractionEnabled = true
        phoneField.becomeFirstResponder()
    }
    
    @IBAction func addressEditTapped(sender: AnyObject) {
        
        address1Field.userInteractionEnabled = true
        address2Field.userInteractionEnabled = true
        cityField.userInteractionEnabled = true
        zipCodeField.userInteractionEnabled = true
        btnState.userInteractionEnabled = true
        
        address1Field.becomeFirstResponder()
    }
    
    @IBAction func stateTapped(sender: UIButton) {
        
//        ActionSheetStringPicker.showPickerWithTitle("State", rows: CommonUtils.getStateList(), initialSelection: 0, doneBlock: { (
//            picker, index, value) in
//            
//            self.btnState.setTitle(value as? String, forState: .Normal)
//            
//            }, cancelBlock: { ActionStringCancelBlock in return } , origin: sender)
        
        self.view.endEditing(true)
        
        if dropDown.hidden {
            
            dropDown.show()
            
        } else {
            
            dropDown.hide()
        }
    }
    
    @IBAction func submitTapped(sender: AnyObject) {
        
        JLToast.makeText("Submit button pressed", duration: 1.0).show()
    }
    
    @IBAction func cancelTapped(sender: AnyObject) {
    
        JLToast.makeText("Cancel button pressed", duration: 1.0).show()
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

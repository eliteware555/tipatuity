//
//  UserProfileEmbeddedViewController.swift
//  Tipatuity
//
//  Created by Victory on 15/06/16.
//  Copyright © 2016 elite. All rights reserved.
//

import UIKit

class UserProfileEmbeddedViewController: BaseViewController {
    
    var userIamge: String!
    
    var isVisibleBadge: Bool = true
    
    @IBOutlet weak var imvProfile: NZCircularImageView!
    @IBOutlet weak var imvBadgeSuper: UIImageView!
    var badgeView: M13BadgeView!

    override func viewDidLoad() {
        
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        if let _ = userIamge {
            
            imvProfile.image = UIImage(named: userIamge)
        }
        
        // badge view
        badgeView = M13BadgeView(frame: CGRectMake(0, 0, 20.0, 20.0))
        badgeView.text = ""
        imvBadgeSuper.addSubview(badgeView)
        
        badgeView.hidesWhenZero = false
        badgeView.maximumWidth = 20.0
        badgeView.font = UIFont.systemFontOfSize(13.0)
        badgeView.horizontalAlignment = M13BadgeViewHorizontalAlignmentCenter
        badgeView.verticalAlignment = M13BadgeViewVerticalAlignmentMiddle

        badgeView.borderWidth = 2
        badgeView.borderColor = UIColor.whiteColor()
        badgeView.badgeBackgroundColor = UIColor(netHex: 0xC6FF00)
 
        badgeView.hidden = !isVisibleBadge
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setUserOnline(online: Bool) {
        
        if (online) {
            
//            badgeView.borderColor = UIColor.whiteColor()
            badgeView.badgeBackgroundColor = UIColor(netHex: 0xC6FF00)
        
        } else {
            
//            badgeView.borderColor = UIColor(netHex: 0xAAAAAA)
            badgeView.badgeBackgroundColor = UIColor(netHex: 0xAAAAAA)
        }
    }
    
    @IBAction func userImageTapped(sender: AnyObject) {
        
    }
    
//    func modalAlertPressed(alert: CustomActionSheet, withButtonIndex: Int) {
//        
//        print("button pressed - \(withButtonIndex)")
//    }
}

extension UIColor {
    convenience init(red: Int, green: Int, blue: Int) {
        assert(red >= 0 && red <= 255, "Invalid red component")
        assert(green >= 0 && green <= 255, "Invalid green component")
        assert(blue >= 0 && blue <= 255, "Invalid blue component")
        
        self.init(red: CGFloat(red) / 255.0, green: CGFloat(green) / 255.0, blue: CGFloat(blue) / 255.0, alpha: 1.0)
    }
    
    convenience init(netHex:Int) {
        self.init(red:(netHex >> 16) & 0xff, green:(netHex >> 8) & 0xff, blue:netHex & 0xff)
    }
}
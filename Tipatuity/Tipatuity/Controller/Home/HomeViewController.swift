//
//  HomeViewController.swift
//  Tipatuity
//
//  Created by Victory on 15/06/16.
//  Copyright © 2016 elite. All rights reserved.
//

import UIKit
import JLToast

class HomeViewController: BaseViewController, CustomActionSheetDelegate, UserViewDelegate {
    
    @IBOutlet weak var bgProfile: UIImageView!
    
    var userProfileVC: UserProfileEmbeddedViewController!
    
    @IBOutlet weak var nearMeSuperView: UIView!
    var nearMeUsers: [UserView] = []
    var arrNearMeUsers: [UserModel] = []
    
    var isOnline: Bool = true
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        initVar()
        
        initView()
    }
    
    func initVar() {
        
        arrNearMeUsers.append(UserModel(name: "MONALISA ABRAR", email: "@monalisaabar", rating: 1.0, image: "user_1", badgeImage: "badge_b", distance: 0.1, isFlag: true))
        arrNearMeUsers.append(UserModel(name: "YAYUM CHOWDHURY", email: "@yayum_chowdhury", rating: 3.0, image: "user_2", badgeImage: "badge_b", distance: 0.3, isFlag: false))
        arrNearMeUsers.append(UserModel(name: "ASIF MAHAMUD", email: "@asif_mahmud", rating: 4.0, image: "user_3", badgeImage: "badge_dollar", distance: 0.3, isFlag: true))
        arrNearMeUsers.append(UserModel(name: "OMAR MOHAMMAD", email: "@omarmohammad", rating: 1.0, image: "user_4", badgeImage: "badge_b", distance: 0.9, isFlag: true))
        arrNearMeUsers.append(UserModel(name: "ABAR KHAYER", email: "@abrar121", rating: 3.0, image: "user_5", badgeImage: "badge_b", distance: 1.0, isFlag: false))
    }
    
    func initView() {

        // generate a tinted unselected image based on image passed via the storyboard
        for item in (self.tabBarController?.tabBar.items)! {

            // use the UIImage category code for the imageWithColor: method
            item.image = item.image?.imageWithRenderingMode(.AlwaysOriginal)
        }
        
        // 59, 20
        
        // blur effect
        let blurEffect = UIBlurEffect(style: UIBlurEffectStyle.Light)
        let blurEffectView = UIVisualEffectView(effect: blurEffect)
        blurEffectView.frame = CGRectMake(0, 0, self.view.bounds.size.width,  bgProfile.bounds.size.height)
        bgProfile.addSubview(blurEffectView)
        
//        print(self.view.bounds.size.width)
        
        let dx = (self.view.frame.size.width - 55) / 5.0
        
        var cx = CGFloat(20)
        
        for index in 0 ..< 5 {
            
            let nearMe = UserView(frame: CGRectMake(cx, 59, dx + 15, dx + 15))
            nearMe.userImage = UIImage(named: arrNearMeUsers[index].image)!
            nearMe.delegate = self
            nearMe.cornerRadius = (dx + 15) / 2.0
            nearMe.borderColor = UIColor.whiteColor()
            nearMe.borderWidth = 2.0
            nearMe.badgeBorderWidth = 1.0
            nearMe.badgeBorderColor = UIColor.whiteColor()
            nearMe.badgeCornerRadius = 10
            nearMe.tag = 200 + index
            nearMeSuperView.addSubview(nearMe)
            nearMeUsers.append(nearMe)
            
            cx = cx + dx
        }
        
        nearMeUsers[0].isVisisbleBadgeView = false
        nearMeUsers[3].isVisisbleBadgeView = false
        
        nearMeUsers[1].badgeBackgroundColor = UIColor(netHex: 0x00BBFF)
        nearMeUsers[1].badgeImage = UIImage(named: "badge_b")!
        nearMeUsers[2].badgeBackgroundColor = UIColor(netHex: 0xF5A623)
        nearMeUsers[2].badgeImage = UIImage(named: "badge_dollar")!
        nearMeUsers[4].badgeBackgroundColor = UIColor(netHex: 0xF5A623)
        nearMeUsers[4].badgeImage = UIImage(named: "badge_dollar")!
        
        self.navigationController?.navigationBar.translucent = false
        
        // remove navigation bar 1px bottom line
        let image = UIImage()
        self.navigationController?.navigationBar.shadowImage = image
        self.navigationController?.navigationBar.setBackgroundImage(image, forBarMetrics: .Default)
        
        // customize back bar button item
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .Plain, target: nil, action: nil)
        navigationController?.navigationBar.tintColor = UIColor.whiteColor()
        
//        print(bgProfile.bounds.size.height)        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func switchUserState(sender: UIButton) {
        
        if (isOnline) {
            
            let actionSheet = CustomActionSheet(delegate: self, buttonTitles: ["Go Idle", "Cancel"], images: ["ic_offline", "ic_cancel"], startIndex: 0)
            actionSheet.showAlert()
            
        } else {
            
            let actionSheet = CustomActionSheet(delegate: self, buttonTitles: ["Go Active", "Cancel"], images: ["ic_online", "ic_cancel"], startIndex: 0)
            actionSheet.showAlert()
        }
    }
    
    // MARK: - IBAction
    @IBAction func settingTapped(sender: AnyObject) {
        
        let actionSheet = CustomActionSheet(delegate: self, buttonTitles: ["Invite Friends", "Settings", "Support", "Cancel"], images: ["ic_share", "ic_setting", "ic_support", "ic_cancel"], startIndex: 2)
        actionSheet.showAlert()
    }
    
    func modalAlertPressed(alert: CustomActionSheet, withButtonIndex: Int) {
        
//        print("button pressed - \(withButtonIndex)")
        
        switch withButtonIndex {
            
        case 0:
            
            if (isOnline) {
                
                userProfileVC.setUserOnline(false)
                isOnline = false
                
            } else {
                
                userProfileVC.setUserOnline(true)
                isOnline = true
            }
            
            break
            
        case 1:
            
            break
            
        case 2:
            
            JLToast.makeText("Invite Freidns Pressed", duration: 1.5).show()
            break
            
        case 3:
            let settingVC = self.storyboard?.instantiateViewControllerWithIdentifier("SettingNav") as! UINavigationController
            
            self.presentViewController(settingVC, animated: true, completion: nil)
            break
            
        case 4:
            JLToast.makeText("Support Pressed", duration: 1.5).show()
            break
            
        default:
            break
        }
    }
    
    // MARK: - UserView Delegate
    func userTapped(sender: UserView) {
        
        let selectedUserIndex = sender.tag - 200
        
//        print(selectedUserIndex)
        
        let destVC = self.storyboard?.instantiateViewControllerWithIdentifier("UserProfileViewController") as! UserProfileViewController
        
        destVC.user = arrNearMeUsers[selectedUserIndex]
        
        destVC.hidesBottomBarWhenPushed = true
        
        self.navigationController?.pushViewController(destVC, animated: true)
    }
    
    
    // MARK: - 
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        
        if (segue.identifier == "SegueEmbeddedOfUserProfile") {
            
            userProfileVC = segue.destinationViewController as! UserProfileEmbeddedViewController
            
        }
    }
}

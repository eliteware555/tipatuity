//
//  SearchUserViewController.swift
//  Tipatuity
//
//  Created by Victory on 20/06/16.
//  Copyright © 2016 elite. All rights reserved.
//

import UIKit
import JLToast

class SearchUserViewController: BaseViewController, UISearchBarDelegate, UITableViewDataSource, UITableViewDelegate, CustomActionSheetDelegate {
    
    @IBOutlet weak var searchField: UISearchBar!
    
    var arrUsers: [UserModel] = []
    
    var arrFilteredUsers: [UserModel] = []
    
    @IBOutlet weak var tblUsers: UITableView!
    
    var searchActivie: Bool = false

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        initVar()
        
        initView()
    }
    
    func initVar() {
        
        arrUsers.append(UserModel(name: "MONALISA ABRAR", email: "monalisaabar", rating: 1.0, image: "user_1", badgeImage: "badge_b", distance: 0.1, isFlag: true))
        arrUsers.append(UserModel(name: "YAYUM CHOWDHURY", email: "yayum_chowdhury", rating: 3.0, image: "user_2", badgeImage: "badge_b", distance: 0.2, isFlag: false))
        arrUsers.append(UserModel(name: "ASIF MAHAMUD", email: "asif_mahmud", rating: 4.0, image: "user_3", badgeImage: "badge_dollar", distance: 0.2, isFlag: true))
        arrUsers.append(UserModel(name: "OMAR MOHAMMAD", email: "omarmohammad", rating: 1.0, image: "user_4", badgeImage: "badge_b", distance: 0.3, isFlag: true))
        arrUsers.append(UserModel(name: "ABAR KHAYER", email: "abrar121", rating: 3.0, image: "user_5", badgeImage: "badge_b", distance: 0.9, isFlag: false))
        arrUsers.append(UserModel(name: "ABAR KAYUM", email: "abrarkayum", rating: 4.0, image: "user_6", badgeImage: "badge_b", distance: 1, isFlag: false))
    }
    
    func initView() {
        
        // customize search bar
        searchField.layer.borderWidth = 1
        searchField.layer.borderColor = UIColor(netHex: 0x039BE5).CGColor
        
        // customize back bar button item
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .Plain, target: nil, action: nil)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - UISearchBarDelegate
    func searchBarSearchButtonClicked(searchBar: UISearchBar) {
        
        searchBar.resignFirstResponder()
        
        if (searchBar.text?.characters.count == 0) {
            
            searchActivie = false
            
        } else {
            
            filterUser(searchBar.text!)
            
            searchActivie = true
        }
        
        tblUsers.reloadData()
    }
    
    func searchBar(searchBar: UISearchBar, textDidChange searchText: String) {
        
        if (searchText.characters.count == 0) {
            
            searchActivie = false
            
        } else {
        
            filterUser(searchText)
            
            searchActivie = true
        }
        
        tblUsers.reloadData()
    }
        
    // MARK: - UITableViewDataSource
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if searchActivie {
           
            return arrFilteredUsers.count
            
        } else {
            
            return arrUsers.count
        }
    }

    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCellWithIdentifier("UserCell") as! UserCell
        
        if (searchActivie) {
            
            cell.setUerModel(arrFilteredUsers[indexPath.row])
            
        } else {
            
            cell.setUerModel(arrUsers[indexPath.row])
        }
        
        return cell
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        
        return 90.0
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        self.searchField.resignFirstResponder()
    }
    
    func filterUser(searchText: String) {
        
        arrFilteredUsers = arrUsers.filter({ (user: UserModel) -> Bool in
            
            let nameMatch = user.name!.rangeOfString(searchText, options: NSStringCompareOptions.CaseInsensitiveSearch)
            
            return nameMatch != nil
        })
    }
    
    @IBAction func settingMenuTapped(sender: AnyObject) {
        
        let actionSheet = CustomActionSheet(delegate: self, buttonTitles: ["Invite Friends", "Settings", "Support", "Cancel"], images: ["ic_share", "ic_setting", "ic_support", "ic_cancel"], startIndex: 0)
        actionSheet.showAlert()
    }
    
    func modalAlertPressed(alert: CustomActionSheet, withButtonIndex: Int) {
        
        //        print("button pressed - \(withButtonIndex)")
        
        switch withButtonIndex {
            
        case 0:
            
            JLToast.makeText("Invite Freidns Pressed", duration: 1.5).show()
            break
            
        case 1:
            
            let settingVC = self.storyboard?.instantiateViewControllerWithIdentifier("SettingNav") as! UINavigationController
            
            self.presentViewController(settingVC, animated: true, completion: nil)
            
            break
            
        case 2:
            
            JLToast.makeText("Support Pressed", duration: 1.5).show()
            break
            
        default:
            break
        }
    }
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        
        if (segue.identifier == "SegueSearch2UserProfile") {
            
            let destVC = segue.destinationViewController as! UserProfileViewController
            
            if let selectedIndexPath = tblUsers.indexPathForSelectedRow {
            
                if (searchActivie) {
                
                    destVC.user = arrFilteredUsers[selectedIndexPath.row]
                    
                } else {
                    
                    destVC.user = arrUsers[selectedIndexPath.row]
                }
            }
        }
    }
 

}

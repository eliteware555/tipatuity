//
//  UserProfileViewController.swift
//  Tipatuity
//
//  Created by Victory on 22/06/16.
//  Copyright © 2016 elite. All rights reserved.
//

import UIKit
import JLToast

class UserProfileViewController: BaseViewController, UITableViewDataSource, UITableViewDelegate, CustomActionSheetDelegate {
    
    @IBOutlet weak var bgProfile: UIImageView!
    
    var user: UserModel!
    
    @IBOutlet weak var ratingView: FloatRatingView!
//    @IBOutlet weak var lblDistance: UILabel!
    
    @IBOutlet weak var tblReviews: UITableView!
    
    @IBOutlet weak var lblUserName: UILabel!
    
    var reviews: [ReviewModel] = []

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        initVar()
        
        initView()
    }
    
    func initVar() {
        
        let user_1 = UserModel(name: "MONALISA ABRAR", email: "monalisaabrar", rating: 1.0, image: "user_1", badgeImage: "badge_b", distance: 0.1, isFlag: true)
        
        let user_2 = UserModel(name: "YAYUM CHOWDHURY", email: "yayum_chowdhury", rating: 3.0, image: "user_2", badgeImage: "badge_b", distance: 0.2, isFlag: false)
        
        reviews.append(ReviewModel(user: user_1, location: "LOCATION", country: "NYC, USA", rating: 1.0, review: "Lorem ipsum dolor site amet, consectetur adipisicing elit, sed do eiusmod tempor."))
        
        reviews.append(ReviewModel(user: user_2, location: "LOCATION", country: "DHAKA, BANGLADESH", rating: 3.0, review: "Great service provider."))
    }
    
    func initView() {
        
        bgProfile.image = UIImage(named: user.image)!
        
        // blur effect
        let blurEffect = UIBlurEffect(style: UIBlurEffectStyle.Dark)
        let blurEffectView = UIVisualEffectView(effect: blurEffect)
        blurEffectView.frame = CGRectMake(0, 0, self.view.bounds.size.width,  bgProfile.bounds.size.height + 5)
        bgProfile.addSubview(blurEffectView)
        
        if let _ = user {
            
            lblUserName.text = user.name
            ratingView.rating = user.rating
//            lblDistance.text = "< " + "\(user.distance)" + " MILES"
        }
        
        tblReviews.estimatedRowHeight = 90.0
        tblReviews.rowHeight = UITableViewAutomaticDimension
        
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .Plain, target: nil, action: nil)
    }
    
    override func viewWillAppear(animated: Bool) {
        
        super.viewWillAppear(animated)
        
        self.navigationController?.navigationBar.barTintColor = UIColor(netHex: 0x039BE5)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - UITableViewDelegate, UITableViewDataSource
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return reviews.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCellWithIdentifier("ReviewCell") as! ReviewCell
        
        let review = reviews[indexPath.row]
        
        cell.lblUserName.text = review.user.name
        cell.userImageView.image = UIImage(named: review.user.image)
        cell.lblUserLocationCity.text = review.location
        cell.lblUserCountry.text = review.country
        cell.ratingView.rating = review.rating
        cell.lblReview.text = review.review
        
//        switch indexPath.row {
//        case 0:
//            
//            cell.lblUserName.text = "MONALISA ABRAR"
//            cell.lblUserLocationCity.text = "LOCATION"
//            cell.lblUserCountry.text = "NYC, USA"
//            cell.ratingView.rating = 1.0
//            cell.lblReview.text = "Lorem ipsum dolor site amet, consectetur adipisicing elit, sed do eiusmod tempor."
//            
//            break
//        default:
//            
//            cell.userImageView.image = UIImage(named: "user_2")
//            cell.lblUserName.text = "ABAR KHAYER CHOWDHURY"
//            cell.lblUserLocationCity.text = "LOCATION"
//            cell.lblUserCountry.text = "DHAKA, BANGLADESH"
//            cell.ratingView.rating = 3.0
//            cell.lblReview.text = "Great service provider."
//            
//            break
//        }
        
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        var viewcontrollers = self.navigationController?.viewControllers
        
        viewcontrollers?.removeLast()
        
        let profileVC = self.storyboard?.instantiateViewControllerWithIdentifier("UserProfileViewController") as! UserProfileViewController
        
        profileVC.user = reviews[indexPath.row].user
        
        viewcontrollers?.append(profileVC)
        
        self.navigationController?.setViewControllers(viewcontrollers!, animated: true)
    }
    
    @IBAction func usernameTapped(sender: AnyObject) {
        
        let actionSheet = CustomActionSheet(delegate: self, buttonTitles: ["Share User Profile", "Bookmark this user", "Report user", "Block user", "Cancel"], images: ["ic_share", "ic_bookmark", "ic_report", "ic_block", "ic_cancel"], startIndex: 4)
        
        actionSheet.showAlert()
    }
    
    // MARK: - IBAction
    @IBAction func menuTapped(sender: AnyObject) {
        
        let actionSheet = CustomActionSheet(delegate: self, buttonTitles: ["Invite Friends", "Settings", "Support", "Cancel"], images: ["ic_share", "ic_setting", "ic_support", "ic_cancel"], startIndex: 0)
        actionSheet.showAlert()
    }
    
    func modalAlertPressed(alert: CustomActionSheet, withButtonIndex: Int) {
        
//        print("button pressed - \(withButtonIndex)")
        
        switch withButtonIndex {
            
        case 0:
            
            JLToast.makeText("Invite Freidns Pressed", duration: 1.5).show()
            break
            
        case 1:
            
            let settingVC = self.storyboard?.instantiateViewControllerWithIdentifier("SettingNav") as! UINavigationController
            
            self.presentViewController(settingVC, animated: true, completion: nil)
            
            break
            
        case 2:
            
            JLToast.makeText("Support Pressed", duration: 1.5).show()
            break
            
        case 4:
            
            JLToast.makeText("Share User Profile Pressed", duration: 1.5).show()
            break
            
        case 5:
            
            JLToast.makeText("Bookmark this user Pressed", duration: 1.5).show()
            break
            
        case 6:
            
            JLToast.makeText("Report user Pressed", duration: 1.5).show()
            break
            
        case 7:
            
            JLToast.makeText("Block user Pressed", duration: 1.5).show()
            break
            
        default:
            break
        }
    }
    
    @IBAction func dollarTapped(sender: AnyObject) {
        
//        var viewcontrollers = self.navigationController?.viewControllers
//        
//        let tipUserVC = self.storyboard?.instantiateViewControllerWithIdentifier("TipUserViewController") as! TipUserViewController
//        
//        let tipJarUser = TipActiveUserModel(userName: user.name, userImage: user.image, userEmail: user.email, rate: user.rating, isOnline: true)
//        
//        tipUserVC.tipUser = tipJarUser
//        
//        viewcontrollers?.append(tipUserVC)
//        
//        let tipUserInputVC = self.storyboard?.instantiateViewControllerWithIdentifier("TipInputViewController") as! TipInputViewController
//        
//        viewcontrollers?.append(tipUserInputVC)
        
//        navigationItem.backBarButtonItem = UIBarButtonItem(title: "TipJar", style: .Plain, target: nil, action: nil)
        
//        self.navigationController?.setViewControllers(viewcontrollers!, animated: true)
    }
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        
        if (segue.identifier == "SegueUserProfile2Embedded") {
            
            let destVC = segue.destinationViewController as! UserProfileEmbeddedViewController
            
            destVC.userIamge = self.user.image
            
            destVC.isVisibleBadge = true
            
        } else if (segue.identifier == "SegueProfile2TipJarInput") {
            
            let destVC = segue.destinationViewController as! TipInputViewController
            destVC.tipUser = self.user
            
//            navigationItem.backBarButtonItem = UIBarButtonItem(title: "Tip User", style: .Plain, target: nil, action: nil)
        }
    }
 

}

//
//  StatisticsViewController.swift
//  Tipatuity
//
//  Created by Victory on 20/06/16.
//  Copyright © 2016 elite. All rights reserved.
//

import UIKit
import JLToast

class StatisticsViewController: BaseTableViewController, CustomActionSheetDelegate {
    
    @IBOutlet weak var ratingSuperView: UIView!
    @IBOutlet weak var ratingView: FloatRatingView!
    
    @IBOutlet weak var incomingChartView: UIView!
    @IBOutlet weak var outgoingChartView: UIView!

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        initView()
    }
    
    func initView() {
        
        // customize back bar button item
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .Plain, target: nil, action: nil)
        navigationController?.navigationBar.tintColor = UIColor.whiteColor()
        
        ratingView.userInteractionEnabled = false
        
        ratingSuperView.layer.borderColor = UIColor(hex: 0xE8E7EC).CGColor
        
        // draw chart
        let labelSettings = ChartLabelSettings(font: ChartDefaultSetting.labelFont)
        
        let incomingChartPoints = [(0, 15), (2, 0), (4, 35), (6, 18), (8, 50)].map{ChartPoint(x: ChartAxisValueInt($0.0, labelSettings: labelSettings), y: ChartAxisValueInt($0.1))}
        
        let outgoingChartPoints = [(0, 13), (1, 17), (3, 48), (5, 12), (8, 32)].map{ChartPoint(x: ChartAxisValueInt($0.0, labelSettings: labelSettings), y: ChartAxisValueInt($0.1))}
        
        let allInComingChartPoints = (incomingChartPoints).sort {
            
            (obj1, obj2) in return obj1.x.scalar < obj2.x.scalar
        }
        
//        let allOutGoingChartPoints = (outgoingChartPoints).sort {
//            
//            (obj1, obj2) in return obj1.x.scalar < obj2.x.scalar
//        }
        
        let xValues: [ChartAxisValue] = (NSOrderedSet(array: allInComingChartPoints).array as! [ChartPoint]).map{$0.x}
        let yValues = ChartAxisValuesGenerator.generateYAxisValuesWithChartPoints(allInComingChartPoints, minSegmentCount: 1, maxSegmentCount: 1, multiple: 50, axisValueGenerator: {ChartAxisValueDouble($0, labelSettings: labelSettings)}, addPaddingSegmentIfEdge: false)
        
        let xModel = ChartAxisModel(axisValues: xValues, axisTitleLabel: ChartAxisLabel(text: "Axis title", settings: labelSettings))
        let yModel = ChartAxisModel(axisValues: yValues, axisTitleLabel: ChartAxisLabel(text: "Axis title", settings: labelSettings.defaultVertical()))
        
        let chartFrame = ChartDefaultSetting.chartFrame(CGRectMake(0, 0, self.view.bounds.size.width-25 , 100))
        
        let chartSettings = ChartDefaultSetting.chartSettings
        chartSettings.trailing = 20
        chartSettings.leading = -40
        chartSettings.bottom = -10
        
        let coordsSpace = ChartCoordsSpaceLeftBottomSingleAxis(chartSettings: chartSettings, chartFrame: chartFrame, xModel: xModel, yModel: yModel)
        let (xAxis, yAxis, innerFrame) = (coordsSpace.xAxis, coordsSpace.yAxis, coordsSpace.chartInnerFrame)
        
        let incomingColor = UIColor(hex: 0x00BBFF)
        let imcomingChartPointLayer = ChartPointsAreaLayer(xAxis: xAxis, yAxis: yAxis, innerFrame: innerFrame, chartPoints: incomingChartPoints, areaColor: incomingColor, animDuration: 0, animDelay: 0, addContainerPoints: true)
        
        let outgoingColor = UIColor(hex: 0x8BC34A)
        let outgoingChartPointLayer = ChartPointsAreaLayer(xAxis: xAxis, yAxis: yAxis, innerFrame: innerFrame, chartPoints: outgoingChartPoints, areaColor: outgoingColor, animDuration: 0, animDelay: 0, addContainerPoints: true)
        
        
        let incomingChart = Chart(frame: chartFrame, layers: [imcomingChartPointLayer ])
        let outgoingChart = Chart(frame: chartFrame, layers: [outgoingChartPointLayer])
        
        self.incomingChartView.addSubview(incomingChart.view)
        self.outgoingChartView.addSubview(outgoingChart.view)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func settingMenuTapped(sender: AnyObject) {
        
        let actionSheet = CustomActionSheet(delegate: self, buttonTitles: ["Invite Friends", "Settings", "Support", "Cancel"], images: ["ic_share", "ic_setting", "ic_support", "ic_cancel"], startIndex: 0)
        actionSheet.showAlert()
    }
    
    func modalAlertPressed(alert: CustomActionSheet, withButtonIndex: Int) {
        
        //        print("button pressed - \(withButtonIndex)")
        
        switch withButtonIndex {
            
        case 0:
            
            JLToast.makeText("Invite Freidns Pressed", duration: 1.5).show()
            break
            
        case 1:
            
            let settingVC = self.storyboard?.instantiateViewControllerWithIdentifier("SettingNav") as! UINavigationController
            
            self.presentViewController(settingVC, animated: true, completion: nil)
            
            break
            
        case 2:
            
            JLToast.makeText("Support Pressed", duration: 1.5).show()
            break
            
        default:
            break
        }
    }
}






//
//  CreateTipJarViewController.swift
//  Tipatuity
//
//  Created by Victory on 18/06/16.
//  Copyright © 2016 elite. All rights reserved.
//

import UIKit
import JLToast

class CreateTipJarViewController: BaseTableViewController, UITextFieldDelegate, UITextViewDelegate, UINavigationControllerDelegate, UIImagePickerControllerDelegate, CustomActionSheetDelegate {
    
    // saved photo path
    var strPhotoPath: String = ""
    
    // UIImagePickerController
    var picker: UIImagePickerController? = UIImagePickerController()
    
    @IBOutlet weak var jarNameField: UITextField!
    @IBOutlet weak var jarUserNameField: UITextField!
    
    @IBOutlet weak var jarSloganView: UITextView!
    @IBOutlet weak var lblLetterCounter: UILabel!
    
    @IBOutlet weak var imvJarImage: NZCircularImageView!

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        initView()
    }
    
    func initView() {
        
        imvJarImage.borderWidth = 1
        imvJarImage.borderColor = UIColor(hex: 0xE8E7EC)
        
        lblLetterCounter.text = "\(jarSloganView.text.characters.count) / 40"
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - UITextFieldDelegate
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        
        textField.resignFirstResponder()
        
        return true
    }
    
    // MARK: - UITextViewDelegate
    func textViewDidChange(textView: UITextView) {
        
        lblLetterCounter.text = "\(textView.text.characters.count) / 40"
        
        // end editing
        if (textView.text.characters.count == 40) {
            
            textView.resignFirstResponder()
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    // MARK: - UIImagePickerControllerDelgeate
    // open gallery
    func openGallery() {
        
        picker?.sourceType = UIImagePickerControllerSourceType.PhotoLibrary
        picker?.allowsEditing = true
        picker?.delegate = self
        self.presentViewController(picker!, animated: true, completion: nil)
    }
    
    // open camera
    func openCamera() {
        
        if(UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.Camera)) {
            
            picker?.sourceType = UIImagePickerControllerSourceType.Camera
            picker?.allowsEditing = true
            picker?.modalPresentationStyle = .FullScreen
            picker?.delegate = self
            
            self.presentViewController(picker!, animated: true, completion: nil)
        }
    }
    
    // MARK: - UIImagePickerController Delegate
    func imagePickerControllerDidCancel(picker: UIImagePickerController) {
        
        dismissViewControllerAnimated(true, completion: nil)
    }
    
    func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : AnyObject]) {
        
        if let newImage = info["UIImagePickerControllerEditedImage"] as? UIImage {
            
            strPhotoPath = CommonUtils.saveToFile(newImage)
            
            imvJarImage.contentMode = .ScaleAspectFit
            imvJarImage.image = UIImage(contentsOfFile: strPhotoPath)
            
        }
        
        picker.dismissViewControllerAnimated(true, completion: nil)
    }
    
    @IBAction func cancelTapped(sender: UIButton) {
        
        
    }
    
    @IBAction func createTapped(sender: UIButton) {
        
    }
    
    @IBAction func addOrChangeTapped(sender: UIButton) {
        
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: .ActionSheet)
        let cameraAction = UIAlertAction(title: "Camera", style: .Default) { (alert: UIAlertAction) -> Void in
            self.openCamera()
        }
        let albumAction = UIAlertAction(title: "Gallery", style: .Default) { (alert: UIAlertAction!) -> Void in
            self.openGallery()
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: .Cancel) { (alert: UIAlertAction!) -> Void in
            
        }
        
        picker?.delegate = self
        
        alert.addAction(cameraAction)
        alert.addAction(albumAction)
        alert.addAction(cancelAction)
        presentViewController(alert, animated: true, completion: nil)
    }
    
    @IBAction func removeTapped(sender: UIButton) {
        
        imvJarImage.image = UIImage()
    }
    
    @IBAction func settingMenuTapped(sender: AnyObject) {
        
        let actionSheet = CustomActionSheet(delegate: self, buttonTitles: ["Invite Friends", "Settings", "Support", "Cancel"], images: ["ic_share", "ic_setting", "ic_support", "ic_cancel"], startIndex: 0)
        actionSheet.showAlert()
    }
    
    func modalAlertPressed(alert: CustomActionSheet, withButtonIndex: Int) {
        
        //        print("button pressed - \(withButtonIndex)")
        
        switch withButtonIndex {
            
        case 0:
            
            JLToast.makeText("Invite Freidns Pressed", duration: 1.5).show()
            break
            
        case 1:
            
            let settingVC = self.storyboard?.instantiateViewControllerWithIdentifier("SettingNav") as! UINavigationController
            
            self.presentViewController(settingVC, animated: true, completion: nil)
            
            break
            
        case 2:
            
            JLToast.makeText("Support Pressed", duration: 1.5).show()
            break
            
        default:
            break
        }
    }

}

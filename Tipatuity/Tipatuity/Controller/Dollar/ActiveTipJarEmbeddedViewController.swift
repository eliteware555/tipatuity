//
//  ActiveTipJarEmbeddedViewController.swift
//  Tipatuity
//
//  Created by Victory on 01/07/16.
//  Copyright © 2016 elite. All rights reserved.
//

import UIKit

class ActiveTipJarEmbeddedViewController: UIViewController {
    
    var tipJar: TipJarModel!
    
    @IBOutlet weak var imvTipJar: NZCircularImageView!
    @IBOutlet weak var imvBadge: UIImageView!
    
    var badgeView: M13BadgeView!

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        if  let _ = tipJar.tipImage {
            
            imvTipJar.image = UIImage(named: tipJar.tipImage)
        }
        
        // badge view
        badgeView = M13BadgeView(frame: CGRectMake(0, 0, 20.0, 20.0))
        badgeView.text = ""
        imvBadge.addSubview(badgeView)
        
        badgeView.hidesWhenZero = false
        badgeView.maximumWidth = 20.0
        badgeView.font = UIFont.systemFontOfSize(13.0)
        badgeView.horizontalAlignment = M13BadgeViewHorizontalAlignmentCenter
        badgeView.verticalAlignment = M13BadgeViewVerticalAlignmentMiddle
        
        badgeView.borderWidth = 2
        badgeView.borderColor = UIColor.whiteColor()
        badgeView.badgeBackgroundColor = UIColor(netHex: 0xC6FF00)

        switchState(tipJar.isActivie)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func switchState(isActivie: Bool) {
        
        if (isActivie) {
            
            badgeView.badgeBackgroundColor = UIColor(netHex: 0xC6FF00)
            
        } else {
            
            badgeView.badgeBackgroundColor = UIColor(netHex: 0xAAAAAA)
        }
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

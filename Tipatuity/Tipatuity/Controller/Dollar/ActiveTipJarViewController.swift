//
//  ActiveTipJarViewController.swift
//  Tipatuity
//
//  Created by Victory on 18/06/16.
//  Copyright © 2016 elite. All rights reserved.
//

import UIKit
import JLToast

class ActiveTipJarViewController: BaseViewController, UITableViewDataSource, UITableViewDelegate, CustomActionSheetDelegate {
    
    @IBOutlet weak var imvBg: UIImageView!
    
    var tipJar: TipJarModel!
    
    @IBOutlet weak var lblTipTitle: UILabel!
    @IBOutlet weak var lblTipDescription: UILabel!
    
    @IBOutlet weak var tblTipActivieUsers: UITableView!
    
    var activeUsers: [UserModel] = []
    
    var activeTipJarVC: ActiveTipJarEmbeddedViewController!

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        initVar()
        
        initView()
    }
    
    func initVar() {
        
        activeUsers.append(UserModel(name: "MONALISA ABRAR", email: "monalisaabar", rating: 1.0, image: "user_1", badgeImage: "badge_b", distance: 0.1, isFlag: true))
        activeUsers.append(UserModel(name: "KAYUM CHOWDHURY", email: "yayum_chowdhury", rating: 3.0, image: "user_2", badgeImage: "badge_b", distance: 0.2, isFlag: false))
        activeUsers.append(UserModel(name: "ASIF MAHAMUD", email: "asif_mahmud", rating: 4.0, image: "user_3", badgeImage: "badge_dollar", distance: 0.2, isFlag: true))
        activeUsers.append(UserModel(name: "OMAR MOHAMMAD", email: "omarmohammad", rating: 1.0, image: "user_4", badgeImage: "badge_b", distance: 0.3, isFlag: true))
        activeUsers.append(UserModel(name: "ABAR KHAYER", email: "abrar121", rating: 3.0, image: "user_5", badgeImage: "badge_b", distance: 0.9, isFlag: false))
        activeUsers.append(UserModel(name: "ABAR KAYUM", email: "abrarkayum", rating: 4.0, image: "user_6", badgeImage: "badge_b", distance: 1, isFlag: false))
    }
    
    func initView() {
        
        // navigation title
        navigationItem.title = "Active TipJar"
        
        lblTipTitle.text = tipJar.tipTitle
        lblTipDescription.text = tipJar.tipDescription
        
        // blur effect
        let blurEffect = UIBlurEffect(style: UIBlurEffectStyle.Light)
        let blurEffectView = UIVisualEffectView(effect: blurEffect)
        blurEffectView.frame = CGRectMake(0, 0, self.view.bounds.size.width,  imvBg.bounds.size.height + 5)
        imvBg.addSubview(blurEffectView)
        
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "Tip User", style: .Plain, target: nil, action: nil)
    }
    
    override func viewWillAppear(animated: Bool) {
        
        super.viewWillAppear(animated)
        
        self.navigationController?.navigationBar.barTintColor = UIColor(netHex: 0x039BE5)        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - UITableViewDataSource & Delegate
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return activeUsers.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCellWithIdentifier("TipJarUserCell") as! TipJarUserCell
        
        cell.setActiveUser(activeUsers[indexPath.row])
        
        return cell
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        
        return 90
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        // to do something here
    }
    
    // MARK: - IBAction
    
    @IBAction func tipJarTitleTapped(sender: AnyObject) {
        
        if (tipJar.isActivie == true) {
            
            let actionSheet = CustomActionSheet(delegate: self, buttonTitles: ["Idle", "Share Tip Jar", "Edit Tip Jar", "Delete Tip Jar", "Cancel"], images: ["ic_offline", "ic_share", "ic_edit", "ic_delete", "ic_cancel"], startIndex: 4)
            
            actionSheet.showAlert()
            
        } else {
            
            let actionSheet = CustomActionSheet(delegate: self, buttonTitles: ["Active", "Share Tip Jar", "Edit Tip Jar", "Delete Tip Jar", "Cancel"], images: ["ic_online", "ic_share", "ic_edit", "ic_delete", "ic_cancel"], startIndex: 4)
            
            actionSheet.showAlert()
        }
    }
    
    @IBAction func menuTapped(sender: AnyObject) {
       
        let actionSheet = CustomActionSheet(delegate: self, buttonTitles: ["Invite Friends", "Settings", "Support", "Cancel"], images: ["ic_share", "ic_setting", "ic_support", "ic_cancel"], startIndex: 0)
        actionSheet.showAlert()
    }
    
    func modalAlertPressed(alert: CustomActionSheet, withButtonIndex: Int) {
        
//        print("\(withButtonIndex) button Pressed")
        
        switch withButtonIndex {
            
        case 0:
            
            JLToast.makeText("Invite Freidns Pressed", duration: 1.5).show()
            break
            
        case 1:
            
            let settingVC = self.storyboard?.instantiateViewControllerWithIdentifier("SettingNav") as! UINavigationController
            
            self.presentViewController(settingVC, animated: true, completion: nil)
            
            break
            
        case 2:
            
            JLToast.makeText("Support Pressed", duration: 1.5).show()
            break
            
        case 3:
            // cancel
            break
            
        case 4:
            
            tipJar.isActivie = !tipJar.isActivie
            activeTipJarVC.switchState(tipJar.isActivie)
            break
            
        case 5:
            
            JLToast.makeText("Share TipJar Pressed", duration: 1.5).show()
            break
            
        case 6:
            
            JLToast.makeText("Edit TipJar Pressed", duration: 1.5).show()
            break
            
        case 7:
            
            JLToast.makeText("Delete TipJar Pressed", duration: 1.5).show()
            break
            
        default:
            break
        }
    }
    
    @IBAction func moreButtonTapped(sender: AnyObject) {
        
        JLToast.makeText("more button pressed", duration: 1.0).show()
    }
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        
        if (segue.identifier == "SegueActiveTipJar2TipUser") {
            
            let destVC = segue.destinationViewController as! TipUserViewController
            
            if let selectedIndexPath = tblTipActivieUsers.indexPathForSelectedRow {
            
                destVC.tipUser = activeUsers[selectedIndexPath.row]
            }
            
        } else if (segue.identifier == "SegueActiveTipJar2Embedded") {
            
            let destVC = segue.destinationViewController as! ActiveTipJarEmbeddedViewController
            destVC.tipJar = self.tipJar
            self.activeTipJarVC = destVC
        }
    }
 
}



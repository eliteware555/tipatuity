//
//  TipUserViewController.swift
//  Tipatuity
//
//  Created by Victory on 22/06/16.
//  Copyright © 2016 elite. All rights reserved.
//

import UIKit
import Material
import JLToast

class TipUserViewController: BaseViewController, TextViewDelegate, UITextFieldDelegate, CustomActionSheetDelegate {
    
    var tipUser: UserModel!
    
    @IBOutlet weak var bgProfile: UIImageView!
    
    @IBOutlet weak var lblUserName: UILabel!
    
    @IBOutlet weak var ratingView: FloatRatingView!
    
    @IBOutlet weak var amountField: UITextField!
    
    @IBOutlet weak var reviewText: TextView!
    
    @IBOutlet weak var letterCount: UILabel!
    
    var tipAmount: String!

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        initView()
    }
    
    func initView() {
        
        self.navigationController?.navigationBar.barTintColor = UIColor(netHex: 0x7ED321)
        
        letterCount.text = "\(reviewText.text.characters.count)" + " / 120"
        
        bgProfile.image = UIImage(named: tipUser.image)
        lblUserName.text = tipUser.name
        ratingView.rating = tipUser.rating
        
        amountField.text = tipAmount
        
        // blur effect
        let blurEffect = UIBlurEffect(style: UIBlurEffectStyle.Light)
        let blurEffectView = UIVisualEffectView(effect: blurEffect)
        blurEffectView.frame = CGRectMake(0, 0, self.view.bounds.size.width,  bgProfile.bounds.size.height + 5)
        bgProfile.addSubview(blurEffectView)
        
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "TipJar", style: .Plain, target: nil, action: nil)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(animated: Bool) {
        
        super.viewWillAppear(animated)
        
        registerForKeyboardNotifications()
    }
    
    override func viewWillDisappear(animated: Bool) {
        
        super.viewWillDisappear(animated)
        
        deregisrerForKeyboardNotifications()
    }
    
    // MARK: TextViewDelegate
    func textViewDidChange(textView: UITextView) {
        
        if (textView.text.characters.count <= 120) {
            
            letterCount.text = "\(textView.text.characters.count)" + " / 120"
        } else {
            
            textView.resignFirstResponder()
        }
    }
   
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        
        if (segue.identifier == "TipUser2Embedded") {
            
            let destVC = segue.destinationViewController as! UserProfileEmbeddedViewController
            
            destVC.userIamge = self.tipUser.image
            destVC.isVisibleBadge = false
            
        } else if (segue.identifier == "SegueTipUser2TipInput") {
            
            let destVC = segue.destinationViewController as! TipInputViewController
            destVC.tipAmount = Float(self.amountField.text!)!
        }
    }
    
    override func getOffsetYWhenShowKeyboard() -> CGFloat {
        
//        if (amountField.isFirstResponder()) {
//            
//            return 216
        
//        } else {
//            
//            return 70
//        }
        
        return 100
    }
    
    @IBAction func settingMenuTapeed(sedner: AnyObject) {
        
        let actionSheet = CustomActionSheet(delegate: self, buttonTitles: ["Invite Friends", "Settings", "Support", "Cancel"], images: ["ic_share", "ic_setting", "ic_support", "ic_cancel"], startIndex: 0)
        actionSheet.showAlert()
    }
    
    func modalAlertPressed(alert: CustomActionSheet, withButtonIndex: Int) {
        
        //        print("button pressed - \(withButtonIndex)")
        
        switch withButtonIndex {
            
        case 0:
            
            JLToast.makeText("Invite Freidns Pressed", duration: 1.5).show()
            break
            
        case 1:
            
            let settingVC = self.storyboard?.instantiateViewControllerWithIdentifier("SettingNav") as! UINavigationController
            
            self.presentViewController(settingVC, animated: true, completion: nil)
            
            break
            
        case 2:
            
            JLToast.makeText("Support Pressed", duration: 1.5).show()
            break
            
        default:
            break
        }
    }
    
    @IBAction func cancelTapped(sender: AnyObject) {
        
        
    }
    
    @IBAction func submitTapped(sender: AnyObject) {
        
    }
}

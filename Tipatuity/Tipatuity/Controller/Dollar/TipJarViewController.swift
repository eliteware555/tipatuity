//
//  TipJarViewController.swift
//  Tipatuity
//
//  Created by Victory on 18/06/16.
//  Copyright © 2016 elite. All rights reserved.
//

import UIKit
import JLToast

class TipJarViewController: BaseViewController, UITableViewDataSource, UITableViewDelegate, CustomActionSheetDelegate {
    
    @IBOutlet weak var tblTipJar: UITableView!
    
    var backTitle = "Active TipJar"
    
    var arrTipJars: [TipJarModel] = []

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        initVar()
        
        initView()
    }
    
    func initVar() {
        
        arrTipJars.append(TipJarModel(tipImage: "load_Island", tipTitle: "Road Island Fund", tipDescription: "Raising money for local development and homeless people of Road Island State", tipReceived: 30, tipMembers: 2, isActive: false))
        arrTipJars.append(TipJarModel(tipImage: "donation", tipTitle: "Donation for Rifat", tipDescription: "Helping local homeless", tipReceived: 130.40, tipMembers: 42, isActive:  true))
        arrTipJars.append(TipJarModel(tipImage: "paper_delivery", tipTitle: "Paper delivery", tipDescription: "Tips for paper delivery on time everyd", tipReceived: 43.23, tipMembers: 12, isActive: true))
    }
    
    func initView() {
        
        self.navigationController?.navigationBar.translucent = false
        
        // remove navigation bar 1px bottom line
        let image = UIImage()
        self.navigationController?.navigationBar.shadowImage = image
        self.navigationController?.navigationBar.setBackgroundImage(image, forBarMetrics: .Default)
        
        // customize back button
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .Plain, target: nil, action: nil)
        navigationController?.navigationBar.tintColor = UIColor.whiteColor()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: UITableView DataSource and Delegate
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return arrTipJars.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCellWithIdentifier("TipJarCell") as! TipJarCell
        
        switch indexPath.row {
        case 0:
            break
            
        case 1:
            break
            
        default:
            
            cell.brand.imvBadge.hidden = true
            
            break
        }
        
        cell.setTipJar(arrTipJars[indexPath.row])
        
        return cell
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        
        return 91
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
    }
    
    
    @IBAction func moreButtonTapped(sender: AnyObject) {
    
        JLToast.makeText("more button pressed", duration: 1.0).show()
    }
    
    @IBAction func settingMenuTapped(sender: AnyObject) {
        
        let actionSheet = CustomActionSheet(delegate: self, buttonTitles: ["Invite Friends", "Settings", "Support", "Cancel"], images: ["ic_share", "ic_setting", "ic_support", "ic_cancel"], startIndex: 0)
        actionSheet.showAlert()
    }
    
    func modalAlertPressed(alert: CustomActionSheet, withButtonIndex: Int) {
        
        //        print("button pressed - \(withButtonIndex)")
        
        switch withButtonIndex {
            
        case 0:
            
            JLToast.makeText("Invite Freidns Pressed", duration: 1.5).show()
            break
            
        case 1:
            
            let settingVC = self.storyboard?.instantiateViewControllerWithIdentifier("SettingNav") as! UINavigationController
            
            self.presentViewController(settingVC, animated: true, completion: nil)
            
            break
            
        case 2:
            
            JLToast.makeText("Support Pressed", duration: 1.5).show()
            break
            
        default:
            break
        }
    }
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        
        if (segue.identifier == "SegueTipJar2Active") {
            
            let destVC = segue.destinationViewController as! ActiveTipJarViewController
            destVC.tipJar = arrTipJars[(tblTipJar.indexPathForSelectedRow?.row)!]
            
        } else if (segue.identifier == "SegueTipJar2Create") {
            
        } 
    }
 

}

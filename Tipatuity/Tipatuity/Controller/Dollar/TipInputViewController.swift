//
//  TipInputViewController.swift
//  Tipatuity
//
//  Created by Victory on 22/06/16.
//  Copyright © 2016 elite. All rights reserved.
//

import UIKit
import Material
import JLToast

class TipInputViewController: BaseViewController, UITextFieldDelegate, TCDTextFieldDelegate, CustomActionSheetDelegate {
    
    var tipUser: UserModel!
    
    @IBOutlet weak var amountField: TCDTextField!
    
    @IBOutlet weak var okButton: FlatButton!
    @IBOutlet weak var lblOk: UILabel!
    
    var tipAmount: Float = 0.0
    
    @IBOutlet weak var lblDollar: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        initView()
    }
    
    func initView() {
        
        self.navigationController!.navigationBar.topItem!.title = "Tip User";
        
//        amountField.tintColor = UIColor.clearColor()
        
        
        amountField.keyboardType = .NumbersAndPunctuation
        
        amountField.text = "\(tipAmount)"
        
        if (amountField.text == "0.0") {
            
            amountField.text = ""
        }
        
        updateAmountValue(amountField.text!)
        
        showOkayButton(false)
        self.navigationController?.navigationBar.barTintColor = UIColor(netHex: 0x8BC34A)
        
        amountField.tcdDelegate = self
        
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .Plain, target: nil, action: nil)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func showOkayButton(isVisible: Bool) {
        
        okButton.hidden = !isVisible
        lblOk.hidden = !isVisible
    }
    
    override func viewDidAppear(animated: Bool) {
        
        super.viewDidAppear(animated)
        
        amountField.becomeFirstResponder()
    }

    func updateAmountValue(value: String) {
        
        let attrWhiteLight = [NSForegroundColorAttributeName: UIColor.whiteColor(), NSFontAttributeName: UIFont(name: "Roboto-Light", size: 17)!]
        
//        let attrWhiteBold = [NSForegroundColorAttributeName: UIColor.whiteColor(), NSFontAttributeName: UIFont(name: "Roboto-Bold", size: 17)!]
        
        let dollarAndCent = value.componentsSeparatedByString(".")
        var dollar = 0
        var cent = 0
        
        // change dollar
        if (dollarAndCent[0].characters.count > 0) {
            
            dollar = Int(dollarAndCent[0])!
        }
        
        // check it contains point
        if value.rangeOfString(".") != nil {
            
            let centPoint = dollarAndCent[1].characters.count
            
            if centPoint != 0 {
                
                cent = Int(dollarAndCent[1])!
             
                if centPoint == 1 {
                 
                    cent = Int(dollarAndCent[1])! * 10
                }
            }
        }
        
        var strDollarUnit = "DOLLARS"
        var strCentUnit = "CENTS"
        
        if (dollar == 1) {
            
            strDollarUnit = "DOLLAR"
        }
        
        if (cent == 1) {
            
            strCentUnit = "CENT"
        }
        
        // change number to word
        let dollarNumberValue = NSNumber(integer: dollar)
        let centNumberValue = NSNumber(integer: cent)
        
        let formatter = NSNumberFormatter()
        formatter.numberStyle = .SpellOutStyle
        
        let strDollar = formatter.stringFromNumber(dollarNumberValue)
        let strCent = formatter.stringFromNumber(centNumberValue)
        
        let strValue = strDollar!.uppercaseString + " " + strDollarUnit + " " + strCent!.uppercaseString + " " + strCentUnit
        
        let attributedText = NSMutableAttributedString(string: strValue)
        attributedText.addAttributes(attrWhiteLight, range: NSMakeRange((strDollar?.characters.count)! + 1, strDollarUnit.characters.count))
        attributedText.addAttributes(attrWhiteLight, range: NSMakeRange((strDollar?.characters.count)! + strCent!.characters.count + strDollarUnit.characters.count + 3, strCentUnit.characters.count))
        
        lblDollar.attributedText = attributedText
    }
    
    func textFieldDidChanged(sender: TCDTextField) {
        
        if (sender.text?.characters.count != 0) {
        
//            okButton.hidden = false
            showOkayButton(true)
            
            tipAmount = NSString(string: sender.text!).floatValue
            
        } else {
            
//            okButton.hidden = true
            showOkayButton(false)
            
            tipAmount = 0.0
            
            return
        }
        
        updateAmountValue(sender.text!)
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        
        textField.resignFirstResponder()
        
        self.performSegueWithIdentifier("TipInput2TipUser", sender: self)
        
        return true
    }
    
    func textField(textField: UITextField,shouldChangeCharactersInRange range: NSRange,replacementString string: String) -> Bool {
        
        let countdots = textField.text!.componentsSeparatedByString(".").count - 1
        
        if countdots > 0 && string == "." {
            
            return false
        }

        if string.isEmpty { // back key
            
            return true
        }
        
        if let input = textField.text {
            let numberFormatter = NSNumberFormatter()
            let range = input.rangeOfString(numberFormatter.decimalSeparator)
            if let r = range {
                let endIndex = input.startIndex.advancedBy(input.startIndex.distanceTo(r.endIndex))
                let decimals = input.substringFromIndex(endIndex)
                return decimals.characters.count < 2
            }
        }
        
        return true
    }
    
    @IBAction func unwindFromTipUser(segue: UIStoryboardSegue) {
        
        // unwind from TipInputViewController
        if (segue.identifier == "SegueUnwindFromTipUser") {
            
            let sourceVC = segue.sourceViewController as! TipUserViewController
            
            amountField.text = sourceVC.tipAmount
        }
    }
    
    @IBAction func settingMenuTapped(sender: AnyObject) {
        
        self.view.endEditing(true)
        
        let actionSheet = CustomActionSheet(delegate: self, buttonTitles: ["Invite Friends", "Settings", "Support", "Cancel"], images: ["ic_share", "ic_setting", "ic_support", "ic_cancel"], startIndex: 0)
        actionSheet.showAlert()
    }
    
    func modalAlertPressed(alert: CustomActionSheet, withButtonIndex: Int) {
        
        //        print("button pressed - \(withButtonIndex)")
        
        switch withButtonIndex {
            
        case 0:
            
            JLToast.makeText("Invite Freidns Pressed", duration: 1.5).show()
            break
            
        case 1:
            
            let settingVC = self.storyboard?.instantiateViewControllerWithIdentifier("SettingNav") as! UINavigationController
            
            self.presentViewController(settingVC, animated: true, completion: nil)
            
            break
            
        case 2:
            
            JLToast.makeText("Support Pressed", duration: 1.5).show()
            break
            
        default:
            break
        }
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        
        if (segue.identifier == "TipInput2TipUser") {
            
            let destVC = segue.destinationViewController as! TipUserViewController
            destVC.tipUser = self.tipUser
            destVC.tipAmount = self.amountField.text
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
}

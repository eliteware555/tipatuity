//
//  LoginViewController.swift
//  Tipatuity
//
//  Created by Victory on 16/06/16.
//  Copyright © 2016 elite. All rights reserved.
//

import UIKit
import Material

class LoginViewController: BaseViewController {
    
    @IBOutlet weak var imvBackground: UIImageView!
    
    @IBOutlet weak var emailField: TextField!
    @IBOutlet weak var passwordField: TextField!
    
    @IBOutlet weak var scrollView: CustomScrollView!

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        initView()
    }
    
    func initView() {
        
        // navigation title with white color
        self.navigationItem.title = "Welcome to Tipatuity"
        
        // blur effect
        let blurEffect = UIBlurEffect(style: UIBlurEffectStyle.Light)
        let blurEffectView = UIVisualEffectView(effect: blurEffect)
        blurEffectView.frame = CGRectMake(0, 0, self.view.bounds.size.width, self.view.bounds.size.height * 360/600)
        blurEffectView.clipsToBounds = true
        imvBackground.addSubview(blurEffectView)
    }
    
    override func viewWillAppear(animated: Bool) {
        
        super.viewWillAppear(animated)
        
        registerForKeyboardNotifications()
    }
    
    override func viewWillDisappear(animated: Bool) {
        
        super.viewWillDisappear(animated)
        
        deregisrerForKeyboardNotifications()
    }
    
    override func didReceiveMemoryWarning() {
        
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func loginTapped(sender: AnyObject) {
        
        self.view.endEditing(true)
        
        showHUDLoadingViewWithTitle("Logging in", sender: self)
      
        self.performSelector(#selector(gotoHomeView), withObject: nil, afterDelay: 2.5)
    }
    
    @IBAction func forgotTapped(sender: AnyObject) {
        
        self.view.endEditing(true)
    }
    
    func gotoHomeView() {

        hideHUDLoadingView()
        
        UIApplication.sharedApplication().keyWindow?.rootViewController = self.storyboard?.instantiateViewControllerWithIdentifier("MyTabBarViewController") as! MyTabBarViewController
    }
}





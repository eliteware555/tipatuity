//
//  SignupViewController.swift
//  Tipatuity
//
//  Created by Victory on 16/06/16.
//  Copyright © 2016 elite. All rights reserved.
//

import UIKit
import Material

class SignupViewController: BaseTableViewController, UITextFieldDelegate {
    
    @IBOutlet weak var firstNameField: TextField!
    @IBOutlet weak var familyNameField: TextField!
    @IBOutlet weak var ssnField: TextField!
    
    // Date of Birth
    @IBOutlet weak var dayField: UITextField!
    @IBOutlet weak var monthField: UITextField!
    @IBOutlet weak var yearField: UITextField!
    
    // address
    @IBOutlet weak var addressField1: UITextField!
    @IBOutlet weak var addressField2: UITextField!
    @IBOutlet weak var cityField: UITextField!
    @IBOutlet weak var zipField: UITextField!
    @IBOutlet weak var btnState: UIButton!
    
    let dropDown = DropDown()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        initView()
    }
    
    func initView() {
       
        dayField.delegate = self
        monthField.delegate = self
        yearField.delegate = self
        
        addressField1.delegate = self
        addressField2.delegate = self
        cityField.delegate = self
        zipField.delegate = self
        
        dayField.tintColor = UIColor.clearColor()
        monthField.tintColor = UIColor.clearColor()
        yearField.tintColor = UIColor.clearColor()

        dropDown.dataSource = CommonUtils.getStateList()
        
        dropDown.selectionAction = { [unowned self] (index, item) in self.btnState.setTitle(item, forState: .Normal)
        }
        
        dropDown.anchorView = btnState
//        dropDown.bottomOffset = CGPoint(x: 0, y: self.btnState.frame.origin.y)
//        dropDown.topOffset = CGPoint(x: 0, y: self.btnState.frame.origin.y)

    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - UITextFieldDelegate
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        
        if (textField == dayField || textField == monthField || yearField == textField) {
        
            return false
            
        } else {
            
            return true
        }
    }
    
    func textFieldDidBeginEditing(textField: UITextField) {
    
        if(textField == dayField || textField == monthField || textField == yearField) {
        
            let datePickerView: UIDatePicker = UIDatePicker()
            datePickerView.datePickerMode = UIDatePickerMode.Date

            textField.inputView = datePickerView
            datePickerView.addTarget(self, action: #selector(datePickerValueChanged(_:)), forControlEvents: UIControlEvents.ValueChanged)
        }
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        
        if (textField == firstNameField) {
            
            familyNameField.becomeFirstResponder()
        } else  if (textField == familyNameField) {
            
            ssnField.becomeFirstResponder()
        } else if (textField == addressField1) {
            
            addressField2.becomeFirstResponder()
        } else if (textField == addressField2) {
            
            cityField.becomeFirstResponder()
            
        } else if (textField == cityField) {
            
            zipField.becomeFirstResponder()
        }
        
        textField.resignFirstResponder()
        
        return true
    }

    func datePickerValueChanged(sender: UIDatePicker) {
        
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "dd-MM-yyyy"
        
        let date = sender.date
        
        let calendar = NSCalendar.currentCalendar()
         let components = calendar.components([.Day , .Month , .Year], fromDate: date)

        dayField.text = "\(components.day)"
        monthField.text = "\(components.month)"
        yearField.text = "\(components.year)"
    }
    
    @IBAction func stateTapped(sender: AnyObject) {
        
        self.view.endEditing(true)
        
        if dropDown.hidden {
            
            dropDown.show()
            
        } else {
            
            dropDown.hide()
        }
    }
}






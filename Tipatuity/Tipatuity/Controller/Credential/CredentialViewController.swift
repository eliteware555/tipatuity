//
//  CredentialViewController.swift
//  Tipatuity
//
//  Created by Victory on 16/06/16.
//  Copyright © 2016 elite. All rights reserved.
//

import UIKit

class CredentialViewController: BaseViewController, CarbonTabSwipeNavigationDelegate {

    var items = ["LOG IN", "SIGN UP"]
    
    var carbonTapSwipeNavigation: CarbonTabSwipeNavigation = CarbonTabSwipeNavigation()
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        initView()
    }
    
    func initView() {
        
        // navigation title with white color
        self.title = "Welcome to Tipatuity"
        
        // change status bar style ( white color)
        self.navigationController!.navigationBar.barStyle = .BlackTranslucent;

        self.navigationController?.navigationBar.translucent = false

        // remove navigation bar 1px bottom line
        let image = UIImage()
        self.navigationController?.navigationBar.shadowImage = image
        self.navigationController?.navigationBar.setBackgroundImage(image, forBarMetrics: .Default)
        
        // carbon swipe view
        carbonTapSwipeNavigation = CarbonTabSwipeNavigation(items: items, delegate: self)
        self.swipeStyle()
    }
    
    func swipeStyle() {
        
        carbonTapSwipeNavigation.toolbar.translucent = false
        carbonTapSwipeNavigation.setIndicatorColor(UIColor.whiteColor())
        
        carbonTapSwipeNavigation.toolbar.setBackgroundImage(UIImage(), forToolbarPosition: .Any, barMetrics: .Default)
        carbonTapSwipeNavigation.toolbar.setShadowImage(UIImage(), forToolbarPosition: .Any)
        
        carbonTapSwipeNavigation.setTabBarHeight(44)
        carbonTapSwipeNavigation.setTabExtraWidth(30)
        
        let tabWidth = self.view.bounds.width / 2.0
        
        carbonTapSwipeNavigation.carbonSegmentedControl!.setWidth(tabWidth, forSegmentAtIndex: 0)
        carbonTapSwipeNavigation.carbonSegmentedControl!.setWidth(tabWidth, forSegmentAtIndex: 1)
        
        carbonTapSwipeNavigation.setIndicatorHeight(4)
        carbonTapSwipeNavigation.toolbar.barTintColor = UIColor(netHex: 0x00bbff)
        
        carbonTapSwipeNavigation.setNormalColor(UIColor(netHex: 0x1d576d))
        carbonTapSwipeNavigation.insertIntoRootViewController(self)
        carbonTapSwipeNavigation.setSelectedColor(UIColor.whiteColor(), font: UIFont.boldSystemFontOfSize(17))
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - CarbonTapSwipeNavigationDelegate
    func carbonTabSwipeNavigation(carbonTabSwipeNavigation: CarbonTabSwipeNavigation, viewControllerAtIndex index: UInt) -> UIViewController {
        
        switch index {
            
        case 0:
            
            return self.storyboard?.instantiateViewControllerWithIdentifier("LoginViewController") as! LoginViewController
            
        default:
            
            return self.storyboard?.instantiateViewControllerWithIdentifier("SignupContainerViewController") as! SignupContainerViewController
        }
    }
    
    func carbonTabSwipeNavigation(carbonTabSwipeNavigation: CarbonTabSwipeNavigation, didMoveAtIndex index: UInt) {
        
//        print("Did move at index: ", "\(index)")
    }
}

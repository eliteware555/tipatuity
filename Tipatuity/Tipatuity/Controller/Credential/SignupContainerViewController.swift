//
//  SignupContainerViewController.swift
//  Tipatuity
//
//  Created by Victory on 16/06/16.
//  Copyright © 2016 elite. All rights reserved.
//

import UIKit
import CoreActionSheetPicker

class SignupContainerViewController: BaseViewController, UINavigationControllerDelegate, UIImagePickerControllerDelegate {
    
    // saved photo path
    var strPhotoPath: String = ""
    
    // UIImagePickerController
    var picker: UIImagePickerController? = UIImagePickerController()
    
    @IBOutlet weak var containerA: UIView!
    @IBOutlet weak var containerB: UIView!
    
    @IBOutlet weak var signButton: UIButton!
    
    var reviewPersonalInfoView: ReviewPersonalInfoView!
    var idRequiredView: IDRequiredView!
    var activateStatusView: ActivationStatusView!

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func buttonTapped(sender: UIButton) {
        
        if (sender.titleForState(.Normal) == "SIGN UP") {
            
            UIView.animateWithDuration(0.5, animations: { 
                
                self.containerA.alpha = 1
                self.containerB.alpha = 0
                
                self.signButton.setTitle("NEXT", forState: .Normal)
            })
            
        } else {
            
            showLoadingViewWithTitle("YOUR ACCOUNT IS BEING\nACTIVATED", buttonTitle: "CANCEL")
            
            self.performSelector(#selector(gotoReviewPersonalInfo), withObject: nil, afterDelay: 2.5)
        }
    }
    
    func gotoReviewPersonalInfo() {
        
        if (loadingView != nil) {
            
            hideLoadingView()
            
            showReviewPersonalInfoView()
        }
    }
    
    func showReviewPersonalInfoView() {
        
        if (reviewPersonalInfoView == nil) {
            
            reviewPersonalInfoView = ReviewPersonalInfoView(frame: CGRectMake(0, 0, self.view.bounds.size.width, self.view.bounds.size.height))
            
            reviewPersonalInfoView.delegate = self
        }
        
        reviewPersonalInfoView.showView(self.view)
    }
    
    func hideReviewPersonalInfoView() {
        
        if (reviewPersonalInfoView != nil) {
        
            reviewPersonalInfoView.hideView()
            reviewPersonalInfoView = nil
        }
    }
    
    func showIDRequiredView() {
        
        if (idRequiredView == nil) {
            
            idRequiredView = IDRequiredView(frame: CGRectMake(0, 0, self.view.bounds.size.width, self.view.bounds.size.height))
            
            idRequiredView.idRequiredDelegate = self
        }
        
        idRequiredView.showView(self.view)
    }
    
    func hideIDRequiredView() {
        
        if (idRequiredView != nil) {
        
            idRequiredView.hideView()
            idRequiredView = nil
        }
    }
    
    func showActivationStatusView() {
        
        hideHUDLoadingView()
        
        hideIDRequiredView()
        
        if (activateStatusView == nil) {
            
            activateStatusView = ActivationStatusView(frame: CGRectMake(0, 0, self.view.bounds.size.width, self.view.bounds.size.height))
            activateStatusView.activationStatusDelegate =  self
        }
        
        activateStatusView.showView(self.view)
    }
    
    func hideActivationStatusView() {
        
        if (activateStatusView != nil) {
        
            activateStatusView.hideView()
            activateStatusView = nil
        }
    }
    
    func gotoHomeView() {
            
        UIApplication.sharedApplication().keyWindow?.rootViewController = self.storyboard?.instantiateViewControllerWithIdentifier("MyTabBarViewController") as! MyTabBarViewController
    }
    
    // MARK: - UIImagePickerControllerDelgeate
    // open gallery
    func openGallery() {
        
        picker?.sourceType = UIImagePickerControllerSourceType.PhotoLibrary
        picker?.allowsEditing = true
        picker?.delegate = self
        self.presentViewController(picker!, animated: true, completion: nil)
    }
    
    // open camera
    func openCamera() {
        
        if(UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.Camera)) {
            
            picker?.sourceType = UIImagePickerControllerSourceType.Camera
            picker?.allowsEditing = true
            picker?.modalPresentationStyle = .FullScreen
            picker?.delegate = self
            
            self.presentViewController(picker!, animated: true, completion: nil)
        }
    }
    
    // MARK: - UIImagePickerController Delegate
    func imagePickerControllerDidCancel(picker: UIImagePickerController) {
        
        dismissViewControllerAnimated(true, completion: nil)
    }
    
    func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : AnyObject]) {
        
        if let newImage = info["UIImagePickerControllerEditedImage"] as? UIImage {
            
            strPhotoPath = CommonUtils.saveToFile(newImage)
            
            if (idRequiredView != nil) {
            
                idRequiredView.imvPhotoID.contentMode = .ScaleAspectFill
                idRequiredView.imvPhotoID.image = UIImage(contentsOfFile: strPhotoPath)
            }
        }
        
        picker.dismissViewControllerAnimated(true, completion: nil)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
}

extension SignupContainerViewController: ReviewPersonalInfoDelegate {
    
    func retryButtonClicked(sender: ReviewPersonalInfoView) {
        
        if (reviewPersonalInfoView != nil) {
            
            sender.hideView()
            
            reviewPersonalInfoView = nil
            
            showIDRequiredView()
        }
    }
}

extension SignupContainerViewController: IDRequiredDelegate {
    
    func onClickAttachLater(sender: IDRequiredView) {
        
        showActivationStatusView()
    }
    
    func onClickUpload(sender: IDRequiredView) {
        
        showHUDLoadingViewWithTitle("UPLOADING", sender: self)
        
        performSelector(#selector(showActivationStatusView), withObject: nil, afterDelay: 3.0)
    }
    
    func onClickChooseOne(sender: IDRequiredView) {
        
        
        if (idRequiredView != nil) {
            
            ActionSheetStringPicker.showPickerWithTitle("Document Type", rows: ["License", "Passport", "ID card", "Other"], initialSelection: 0, doneBlock: { (
                picker, index, value) in
                
                sender.btnChooseOne.setTitle(value as? String, forState: .Normal)
                
                }, cancelBlock: { ActionStringCancelBlock in return } , origin: sender.btnChooseOne)
        }
        
    }
    
    func onClickSnapPhoto(sender: IDRequiredView) {
        
        openCamera()
    }
    
    func onClickOpenGallery(sender: IDRequiredView) {
        
        openGallery()
    }
}

extension SignupContainerViewController: ActivationStatusViewDelegate {
    
    func onClickVerify(sender: ActivationStatusView) {
        
    }
    
    func onClickPersonalInfoUpload(sender: ActivationStatusView) {
        
    }
    
    func onClickLink(sender: ActivationStatusView) {
        
    }
    
    func onClickDone(sender: ActivationStatusView) {
        
        if (activateStatusView != nil) {
            
            sender.hideView()
            
            activateStatusView = nil
            
            gotoHomeView()
        }
    }
}



//
//  TipJarUserCell.swift
//  Tipatuity
//
//  Created by Victory on 19/06/16.
//  Copyright © 2016 elite. All rights reserved.
//

import UIKit
import Material

class TipJarUserCell: UITableViewCell {
    
    @IBOutlet weak var vUser: TipJarUserView!
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var lblEmail: UILabel!
    @IBOutlet weak var vRating: FloatRatingView!
    
    @IBOutlet weak var btnMenu: FlatButton!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setActiveUser(item: UserModel) {
        
        vUser.imvUserImage.image = UIImage(named: item.image)!
        lblUserName.text = item.name
        lblEmail.text = item.email
        vRating.rating = item.rating
        
        if (item.isFlag) {
            
            vUser.imvBadgeView.backgroundColor = UIColor(hex: 0x8BC34A)
            
        } else {
            
            vUser.imvBadgeView.backgroundColor = UIColor(hex: 0xAAAAAA)
        }
    }

}



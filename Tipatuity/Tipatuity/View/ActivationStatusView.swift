//
//  ActivationStatusView.swift
//  Tipatuity
//
//  Created by Victory on 19/06/16.
//  Copyright © 2016 elite. All rights reserved.
//

import UIKit
import Material

protocol ActivationStatusViewDelegate: class {
    
    func onClickVerify(sender: ActivationStatusView)
    func onClickPersonalInfoUpload(sender: ActivationStatusView)
    func onClickLink(sender: ActivationStatusView)
    func onClickDone(sender: ActivationStatusView)
}

class ActivationStatusView: UIView {
    
    private var contentView: UIView!
    
    weak var activationStatusDelegate: ActivationStatusViewDelegate?
    
    // phone number
    @IBOutlet weak var imvStatusPhoneNumber: UIImageView!
    @IBOutlet weak var lblStatusPhoneNumber: UILabel!
    @IBOutlet weak var btnVerify: RaisedButton!
    
    // personal identity
    @IBOutlet weak var imvStatusPersonalIdentity: UIImageView!
    @IBOutlet weak var lblStatusPersonalIdentity: UILabel!
    @IBOutlet weak var btnUpload: RaisedButton!
    
    // bank account
    @IBOutlet weak var imvBankAccount: UIImageView!
    @IBOutlet weak var lblStatusBankAccount: UILabel!
    @IBOutlet weak var btnLink: RaisedButton!
    
    // for use in IB
    required init?(coder aDecoder: NSCoder) {
        
        super.init(coder: aDecoder)
        
        xibSetup()
        
        commonInit()
    }
    
    // for use in Code
    override init(frame: CGRect) {
        
        super.init(frame: frame)
        
        xibSetup()
        
        commonInit()
    }
    
    private func loadFromNib() -> UIView {
        
        let bundle = NSBundle(forClass: self.dynamicType)
        let nib = UINib(nibName: "ActivationStatusView", bundle: bundle)
        let view = nib.instantiateWithOwner(self, options: nil)[0] as! UIView
        
        return view
    }
    
    func xibSetup() {
        
        contentView = loadFromNib()
        contentView.frame = bounds
        contentView.autoresizingMask = [.FlexibleWidth, .FlexibleHeight]
        
        addSubview(contentView)
    }
    
    func commonInit() {
        
        // add blur effect view
        let blurEffect = UIBlurEffect(style: .Dark)
        let blurEffectView = UIVisualEffectView(effect: blurEffect)
        blurEffectView.frame = self.bounds
        
        blurEffectView.autoresizingMask = [.FlexibleWidth, .FlexibleHeight]
        insertSubview(blurEffectView, atIndex: 0)
        
        // 
        setPhoneNumberStatus(true)
    }
    
    func showView(onView: UIView) {
        
        onView.addSubview(self)
    }
    
    func hideView() {
        
        removeFromSuperview()
    }
    
    func setPhoneNumberStatus(status: Bool) {
        
        if (status) {
            
            btnVerify.hidden = true
            
        } else {
            
            btnVerify.hidden = false
        }
    }
    
    func setPersonalIdentityStatus(status: Bool) {
        
    }
    
    func setBankAccountStatus(status: Bool) {
        
    }
    
    @IBAction func verifyTapped(sender: UIButton) {
        
        activationStatusDelegate?.onClickVerify(self)
    }
    
    @IBAction func uploadTapped(sender: UIButton) {
        
        activationStatusDelegate?.onClickPersonalInfoUpload(self)
    }
    
    @IBAction func linkTapped(sender: UIButton) {
        
        activationStatusDelegate?.onClickLink(self)
    }
    
    @IBAction func doneTapped(sender: UIButton) {
        
        activationStatusDelegate?.onClickDone(self)
    }

    /*
    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func drawRect(rect: CGRect) {
        // Drawing code
    }
    */

}

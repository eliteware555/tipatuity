//
//  CustomUserview.h
//  Shehadi
//
//  Created by Victory on 30/05/16.
//  Copyright © 2016 victory. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NZCircularImageView.h"

IB_DESIGNABLE

@interface CustomUserView : UIView

@property (nonatomic) IBInspectable UIImage *userImage;

@property (nonatomic) IBInspectable NSInteger borderWidth;
@property (nonatomic) IBInspectable NSInteger cornerRadious;
@property (nonatomic) IBInspectable UIColor *borderColor;

@property (nonatomic) IBInspectable UIColor *badgeBackColor;
@property (nonatomic) IBInspectable UIImage *badgeImage;

@property (nonatomic) IBInspectable NSInteger badgeViewCornerRadius;
@property (nonatomic) IBInspectable NSInteger badgeViewBorderWidth;

@property (nonatomic, weak) IBOutlet NZCircularImageView *imvUser;

@property (nonatomic, weak) IBOutlet UIImageView *imvIcon;

@end

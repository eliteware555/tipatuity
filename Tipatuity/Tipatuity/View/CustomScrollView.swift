//
//  CustomScrollView.swift
//  Tipatuity
//
//  Created by Victory on 16/06/16.
//  Copyright © 2016 elite. All rights reserved.
//

import UIKit

class CustomScrollView: UIScrollView {
    
    override init(frame: CGRect) {
        
        super.init(frame: frame)
    }

    required init?(coder aDecoder: NSCoder) {
        
        super.init(coder: aDecoder)
    }
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        
        // If not dragging, send event to next responder
        if (!self.dragging) {
            
            self.nextResponder()?.touchesBegan(touches, withEvent: event)
            
        } else {
            
            super.touchesBegan(touches, withEvent: event)
        }
    }
    
    override func touchesMoved(touches: Set<UITouch>, withEvent event: UIEvent?) {
        
        // If not dragging, send event to next responder
        if (!self.dragging) {
            
            self.nextResponder()?.touchesMoved(touches, withEvent: event)
            
        } else {
            
            super.touchesMoved(touches, withEvent: event)
        }
    }
    
    override func touchesEnded(touches: Set<UITouch>, withEvent event: UIEvent?) {
        
        // If not dragging, send event to next responder
        if (!self.dragging) {
            
            self.nextResponder()?.touchesEnded(touches, withEvent: event)
            
        } else {
            
            super.touchesEnded(touches, withEvent: event)
        }
    }
}

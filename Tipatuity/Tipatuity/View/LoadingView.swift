//
//  LoadingView.swift
//  Tipatuity
//
//  Created by Victory on 16/06/16.
//  Copyright © 2016 elite. All rights reserved.
//

import UIKit

protocol LoadingViewDelegate: class {
    func cancelButtonClicked(sender: LoadingView)
}

//  Create LoadingView.xib, set File's owner to LoadingView
//  Link the top level view in the XIB to the contentView outlet

@IBDesignable class LoadingView: UIView {
    
    var nibName = "LoadingView"
    
    @IBOutlet private var contentView: UIView?
    
    @IBOutlet weak var centerView: UIView!
    
    @IBOutlet weak var loadingView: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var button: UIButton!
    
    weak var cancelDelegate: LoadingViewDelegate?
    
//    @IBInspectable var title: String? {
//        
//        get {
//            return lblTitle.text
//        }
//        
//        set (title) {
//            
//            lblTitle.text = title
//        }
//    }
//    
//    @IBInspectable var buttonTitle: String? {
//        
//        get {
//            
//            return button.titleForState(UIControlState.Normal)
//        }
//        
//        set (buttonTitle) {
//            
//            button.setTitle(buttonTitle, forState: .Normal)
//        }
//    }
//    
//    @IBInspectable var cornerRadius: CGFloat = 0 {
//        
//        didSet {
//            
//            self.centerView.layer.cornerRadius = cornerRadius
//            clipsToBounds = true
//        }
//    }
    
    // for using LoadingView in Code
    override init(frame: CGRect) {
        
        super.init(frame: frame)
        // extra function go in after super.init
        
        commonInit()
        
        setup()
    }
    
    
    // for using LoadingView in IB
    required init?(coder aDecoder: NSCoder) {
        
        super.init(coder: aDecoder)
        // extra function go in AFTER super.init
        
        commonInit()
        
        setup()
    }
    
    // setup the view from .xib
    private func commonInit() {
        
        NSBundle.mainBundle().loadNibNamed(nibName, owner: self, options: nil)
        
        guard let content = contentView else { return }
        
        content.frame = self.bounds
        content.autoresizingMask = [.FlexibleHeight, .FlexibleWidth]
        
        self.addSubview(content)
    }
    
    func setup() {
        
        // add blur effect view
        let blurEffect = UIBlurEffect(style: .Dark)
        let blurEffectView = UIVisualEffectView(effect: blurEffect)
        blurEffectView.frame = self.bounds
        
        blurEffectView.autoresizingMask = [.FlexibleWidth, .FlexibleHeight]
        insertSubview(blurEffectView, atIndex: 0)
    }
    
    // show loading view
    func showLoadingWithTitle(title: String, buttonTitle: String, onView: UIView) {
        
        // set title and button title
        self.lblTitle.text = title
        self.button.setTitle(buttonTitle, forState: .Normal)
        
        loadingView.backgroundColor = UIColor.clearColor()
        
        loadingView.animationImages = [UIImage(named: "loading_1")!, UIImage(named: "loading_2")!, UIImage(named: "loading_3")!, UIImage(named: "loading_4")!, UIImage(named: "loading_5")!, UIImage(named: "loading_6")!, UIImage(named: "loading_7")!]
        loadingView.animationDuration = 1.0
        loadingView.animationRepeatCount = 0
        loadingView.startAnimating()
        
        onView.addSubview(self)
    }
    
    // hide loading view
    func hideLoadingView() {
        
        loadingView.stopAnimating()
        removeFromSuperview()
    }
    
    override func prepareForInterfaceBuilder() {
        
        // set up anything that you need
        // this is run before the view is shown in the interface
    }
    
    @IBAction func cancelButtonTapped(sender: UIButton) {
        
        cancelDelegate?.cancelButtonClicked(self)
    }
}

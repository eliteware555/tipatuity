//
//  CustomActionSheet.swift
//  Tipatuity
//
//  Created by Victory on 21/06/16.
//  Copyright © 2016 elite. All rights reserved.
//

import UIKit
import Material

@objc protocol CustomActionSheetDelegate: class {
    
    optional func modalAlertPressed(alert: CustomActionSheet, withButtonIndex: Int)
}

class CustomActionSheet: UIView {

    // this is where we declare our protocol
    var delegate: CustomActionSheetDelegate?
    
    private var backgroundView: UIView!
    private var yPosition: CGFloat!
    private var index: Int!
    
    override init(frame: CGRect) {
        
        super.init(frame: frame)
        
        yPosition = 15.0
    }
    
    // for use in IB
    required init?(coder aDecoder: NSCoder) {
        
        super.init(coder: aDecoder)
        
        yPosition = 15.0
    }
    
    init(delegate: AnyObject, buttonTitles: [String], images: [String], startIndex: Int) {
        
        super.init(frame: CGRectMake(0, 0, UIScreen.mainScreen().bounds.size.width, UIScreen.mainScreen().bounds.size.height))
        
        self.delegate = delegate as? CustomActionSheetDelegate
        
        yPosition = 15.0
        index = startIndex
        
        backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.2)
        
        // add blur effect view
        let blurEffect = UIBlurEffect(style: .Light)
        let blurEffectView = UIVisualEffectView(effect: blurEffect)
        blurEffectView.frame = self.bounds
        
        blurEffectView.autoresizingMask = [.FlexibleWidth, .FlexibleHeight]
        insertSubview(blurEffectView, atIndex: 0)
        
        // set up background view
        backgroundView = UIView()
        backgroundView.backgroundColor = UIColor(hex: 0xFAFAFA)
        backgroundView.userInteractionEnabled = true
        var frame: CGRect = backgroundView.frame
        
        backgroundView.userInteractionEnabled = true
        frame.size = CGSizeMake(self.frame.size.width, 380)
        frame.origin = CGPointMake(0, self.frame.size.height)
        
        backgroundView.frame = frame
        
        // I will be used to set the button's tag and is returned to delegate method
        // loop through the buttons and create a button for each
        for index in 0 ..< buttonTitles.count {
            
            // alertButton
            let alertButton = UIButton(frame: CGRectMake(0, 0, backgroundView.frame.size.width, 36))
            frame = alertButton.frame
            frame.origin.y = yPosition
            
            alertButton.frame = frame
            alertButton.tag = 200 + index + startIndex
            
            alertButton.addTarget(self, action: #selector(buttonPressedWithIndex(_:)), forControlEvents: .TouchUpInside)
            [backgroundView .addSubview(alertButton)]
            
            // alert button icon (UIImageView)
            let imageView = UIImageView(frame: CGRectMake(0, 0, 60, 24))
            frame = imageView.frame
            frame.origin.y = yPosition + 6
            
            imageView.frame = frame
            imageView.contentMode = UIViewContentMode.ScaleAspectFit
            imageView.image = UIImage(named: images[index])
            backgroundView.addSubview(imageView)
            
            // alert button title (UILabel)
            let label = UILabel(frame: CGRectMake(0, 0, backgroundView.frame.size.width - 60, 36))
            frame = label.frame
            frame.origin.x = 60
            frame.origin.y = yPosition
            
            label.frame = frame
            label.textColor = UIColor(hex: 0x5F5F5F)
            label.text = buttonTitles[index]
            label.font = UIFont.systemFontOfSize(16)
            backgroundView.addSubview(label)
            
            yPosition = yPosition + alertButton.frame.size.height + 10
        }
        
        frame = backgroundView.frame
        frame.size.height = yPosition
        backgroundView.frame = frame
        
        // add background view and animate the view on screen
        addSubview(backgroundView)
        
        animateOn()
    }
    
    func animateOn() {
        
        UIView.animateWithDuration(0.23) {
            
            var frame = self.backgroundView.frame
            frame.origin.y -= self.yPosition
            self.backgroundView.frame = frame
        }
    }
    
    func animateOff() {
        
        UIView.animateWithDuration(0.23, animations: { 
            
            var frame = self.backgroundView.frame
            frame.origin.y += self.yPosition
            self.backgroundView.frame = frame
            
            }) { (true) in

                self.delegate?.modalAlertPressed!(self, withButtonIndex: self.index)
                self.removeFromSuperview()
        }
    }
    
    func buttonPressedWithIndex(sender: AnyObject) {
        
        // get the button that was pressed

        let button = sender as! UIButton
        index = button.tag - 200
        
        animateOff()
    }
    
    func showAlert() {
        
        UIApplication.sharedApplication().keyWindow?.addSubview(self)
    }
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        
//        UIView.animateWithDuration(0.23, animations: {
//            
//            var frame = self.backgroundView.frame
//            frame.origin.y += self.yPosition
//            self.backgroundView.frame = frame
//            
//        }) { (true) in
//            
//            self.removeFromSuperview()
//        }
    }

    /*
    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func drawRect(rect: CGRect) {
        // Drawing code
    }
    */
}

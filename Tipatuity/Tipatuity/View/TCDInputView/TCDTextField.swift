//
//  TextField.swift
//  TCDInputView
//
//  Created by Tom Diggle on 21/05/2015.
//  Copyright (c) 2015 Tom Diggle. All rights reserved.
//  http://tomdiggle.com
//

import UIKit

protocol TCDTextFieldDelegate: class {
    
    func textFieldDidChanged(sender: TCDTextField)
}

@IBDesignable
public class TCDTextField: UITextField, KeyboardViewDelegate {
    
    weak var tcdDelegate: TCDTextFieldDelegate?
    
    var keyboardView: KeyboardView?
    @IBInspectable public var noDecimal = false
    @IBInspectable public var noBackSlash = false
    
    override public var inputView: UIView? {
        get {
            if (self.keyboardView != nil) {
                return self.keyboardView
            }
            
            let nibContents = NSBundle.mainBundle().loadNibNamed("KeyboardView", owner: self, options: nil) as Array
            self.keyboardView = nibContents.first as? KeyboardView
            self.keyboardView?.delegate = self
            self.keyboardView?.decimalButton.enabled = !noDecimal
            self.keyboardView?.backSlashButton.enabled = !noBackSlash
            return self.keyboardView
        }
        set { }
    }
    
    // MARK: KeyboardViewDelegate
    
    func returnButtonTapped() {
        self.delegate?.textFieldShouldReturn?(self)
    }
    
    func backspaceButtonTapped() {
        if self.text!.characters.count > 0 {
            self.text = String(self.text!.characters.dropLast())
            tcdDelegate?.textFieldDidChanged(self)
        }
    }
    
    func characterTapped(character: String) {
        
        var appendCharacter = true
        
        if true == delegate?.respondsToSelector(#selector(UITextFieldDelegate.textField(_:shouldChangeCharactersInRange:replacementString:))) {
            appendCharacter = delegate!.textField!(self, shouldChangeCharactersInRange: NSMakeRange(self.text!.characters.count, 1), replacementString: character)
        }
        
        if appendCharacter {
            self.text! += character
        
            tcdDelegate?.textFieldDidChanged(self)
        }
    }
    
}

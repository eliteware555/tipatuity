//
//  UserCell.swift
//  Tipatuity
//
//  Created by Victory on 22/06/16.
//  Copyright © 2016 elite. All rights reserved.
//

import UIKit

class UserCell: UITableViewCell {
    
    @IBOutlet weak var userView: UserView!
    
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var lblUserEmail: UILabel!
    @IBOutlet weak var ratingView: FloatRatingView!
    
    @IBOutlet weak var lblDistance: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setUerModel(user: UserModel) {
        
        userView.userImage = UIImage(named: user.image)!
        userView.badgeImage = (UIImage(named: user.badgeImage)!)
        
        userView.borderWidth = 0
        
        if (user.badgeImage == "badge_b") {
            
            userView.badgeBackgroundColor = UIColor(netHex: 0xF5A623)
            
        } else {
            
            userView.badgeBackgroundColor = UIColor(netHex: 0x00BBFF)
        }
        
        userView.badgeBorderWidth = 2
        userView.badgeBorderColor = UIColor.whiteColor()
        
        if (user.isFlag) {
            
            userView.badgeImageView.hidden = false
            
        } else {
            
            userView.badgeImageView.hidden = true
        }
        
        lblUserName.text = user.name
        lblUserEmail.text = user.email
        ratingView.rating = user.rating
        
        lblDistance.text = "< " + "\(user.distance)" + " MILE"
    }

}

//
//  TipJarUserView.swift
//  Tipatuity
//
//  Created by master on 6/18/16.
//  Copyright © 2016 elite. All rights reserved.
//

import UIKit

@IBDesignable class TipJarUserView: UIView {
    
    private var contentView: UIView!
    
    @IBOutlet weak var imvUserImage: NZCircularImageView!
    
    @IBOutlet weak var imvBadgeView: UIImageView!
    
    @IBInspectable var userImage: UIImage = UIImage() {
        
        didSet {
            
            imvUserImage.image = userImage
        }
    }
    
    @IBInspectable var badgeColor: UIColor = UIColor.greenColor() {
        
        didSet {
            
            imvBadgeView.backgroundColor = badgeColor
        }
    }
    
    @IBInspectable var cornerRadius: CGFloat = 0 {
        
        didSet {
            
            imvBadgeView.layer.cornerRadius = cornerRadius
            imvBadgeView.layer.masksToBounds = true
        }
    }
    
    // for using in IB
    required init?(coder aDecoder: NSCoder) {
        
        super.init(coder: aDecoder)
        
        xibSetup()
    }
    
    override init(frame: CGRect) {
        
        super.init(frame: frame)
        
        xibSetup()
    }
    
    private func loadFromNib() -> UIView {
        
        let bundle = NSBundle(forClass: self.dynamicType)
        let nib = UINib(nibName: "TipJarUserView", bundle: bundle)
        let view = nib.instantiateWithOwner(self, options: nil)[0] as! UIView
        
        return view
    }
    
    private func xibSetup() {
        
        contentView = loadFromNib()
        contentView.frame = frame
        contentView.autoresizingMask = [.FlexibleWidth, .FlexibleHeight]
        
        addSubview(contentView)
    }
    
    override func prepareForInterfaceBuilder() {
        
        super.prepareForInterfaceBuilder()
        
        imvUserImage.image = userImage
        
        imvBadgeView.backgroundColor = badgeColor
        imvBadgeView.layer.cornerRadius = cornerRadius
        imvBadgeView.layer.masksToBounds = true
    }
    
    // for using in Code

    /*
    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func drawRect(rect: CGRect) {
        // Drawing code
    }
    */

}

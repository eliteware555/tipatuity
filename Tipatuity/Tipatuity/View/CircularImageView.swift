//
//  CustomImageView.swift
//  Tipatuity
//
//  Created by Victory on 24/06/16.
//  Copyright © 2016 elite. All rights reserved.
//

import UIKit

@IBDesignable class CircularImageView: UIImageView {
    
    @IBInspectable var cornerRadius: CGFloat = 0 {
        
        didSet {
            
            layer.cornerRadius = cornerRadius
            layer.masksToBounds = true
            
//            print("cornerRadius of CirclularImageView: ------- " + "\(cornerRadius)")
        }
    }
    
    @IBInspectable var borderColor: UIColor = UIColor.whiteColor() {
        
        didSet {
            
            layer.borderColor = borderColor.CGColor
        }
    }
    
    @IBInspectable var borderWidth: CGFloat = 0 {
        
        didSet {
            
            layer.borderWidth = borderWidth
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        
        super.init(coder: aDecoder)
    }
    
    override init(frame: CGRect) {
        
        super.init(frame: frame)
    }

    
    override func prepareForInterfaceBuilder() {
        
        super.prepareForInterfaceBuilder()
        
        layer.cornerRadius = cornerRadius
        layer.borderColor = borderColor.CGColor
        layer.borderWidth = borderWidth
        layer.masksToBounds = true
        clipsToBounds = true
    }
    
    
    /*
    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func drawRect(rect: CGRect) {
        // Drawing code
    }
    */
    
    

}

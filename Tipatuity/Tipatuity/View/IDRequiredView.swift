//
//  IDRequiredView.swift
//  Tipatuity
//
//  Created by Victory on 19/06/16.
//  Copyright © 2016 elite. All rights reserved.
//

import UIKit
import Material

protocol IDRequiredDelegate: class {
    
    func onClickAttachLater(sender: IDRequiredView)
    func onClickUpload(sender: IDRequiredView)
    func onClickChooseOne(sender: IDRequiredView)
    func onClickSnapPhoto(sender: IDRequiredView)
    func onClickOpenGallery(sender: IDRequiredView)
}

@IBDesignable class IDRequiredView: UIView {
    
    private var contentView: UIView!
    
    @IBOutlet weak var infoView: UIView!
    
    @IBOutlet weak var lblDescription: UILabel!
    @IBOutlet weak var imvPhotoID: UIImageView!
    
    @IBOutlet weak var btnClear: UIButton!
    
    @IBOutlet weak var btnChooseOne: UIButton!
    
    weak var idRequiredDelegate: IDRequiredDelegate?
    
    // for use in IB
    required init?(coder aDecoder: NSCoder) {
        
        super.init(coder: aDecoder)
        
        xibSetup()
        
        commonInit()
    }
    
    // for use in Code
    override init(frame: CGRect) {
        
        super.init(frame: frame)
        
        xibSetup()
        
        commonInit()
    }
    
    private func loadFromNib() -> UIView {
        
        let bundle = NSBundle(forClass: self.dynamicType)
        let nib = UINib(nibName: "IDRequiredView", bundle: bundle)
        let view = nib.instantiateWithOwner(self, options: nil)[0] as! UIView
        
        return view
    }
    
    func xibSetup() {
        
        contentView = loadFromNib()
        contentView.frame = bounds
        contentView.autoresizingMask = [.FlexibleWidth, .FlexibleHeight]
        
        addSubview(contentView)
    }
    
    func commonInit() {
        
        // add blur effect view
        let blurEffect = UIBlurEffect(style: .Dark)
        let blurEffectView = UIVisualEffectView(effect: blurEffect)
        blurEffectView.frame = self.bounds
        
        blurEffectView.autoresizingMask = [.FlexibleWidth, .FlexibleHeight]
        insertSubview(blurEffectView, atIndex: 0)
        
        // add info mark at the last of label text (fontawesome)
        lblDescription.font = UIFont.fontAwesomeOfSize(14)
        
        let blueAttribute = [NSForegroundColorAttributeName: UIColor(hex: 0x00BBFF)]
        
        let strDescription = "To help confirm your identify,\nplease attach a copy of your photo id that shows your name and address "
        
        let attributedText = NSMutableAttributedString(string: strDescription + String.fontAwesomeIconWithCode("fa-exclamation-circle")!)
        attributedText.addAttributes(blueAttribute, range:NSMakeRange(strDescription.characters.count, 1))
        
        lblDescription.attributedText = attributedText
        
        lblDescription.attributedText = attributedText
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(tapFunction(_:)))
        lblDescription.addGestureRecognizer(tap)
        
        lblDescription.userInteractionEnabled = true
        
        // photo id corner
        imvPhotoID.layer.borderWidth = 1
        imvPhotoID.layer.borderColor = UIColor(hex: 0xE8E7EC).CGColor
        imvPhotoID.layer.masksToBounds = true
        
        infoView.hidden = true
        infoView.alpha = 0
    }
    
    override func prepareForInterfaceBuilder() {
        
        super.prepareForInterfaceBuilder()
    }
    
    func showView(onView: UIView) {
        
        onView.addSubview(self)
    }
    
    func hideView() {
        
        removeFromSuperview()
    }
    
    @IBAction func chooseOneTapped(sender: UIButton) {
        
        idRequiredDelegate?.onClickChooseOne(self)
    }
    
    @IBAction func snapPhotoTapped(sender: UIButton) {
        
        idRequiredDelegate?.onClickSnapPhoto(self)
    }
    
    @IBAction func uploadGalleryTapped(sender: UIButton) {
        
        idRequiredDelegate?.onClickOpenGallery(self)
    }
    
    @IBAction func attachLaterTapped(sender: UIButton) {
        
        idRequiredDelegate?.onClickAttachLater(self)
    }
    
    @IBAction func uploadTapped(sender: UIButton) {
        
        idRequiredDelegate?.onClickUpload(self)
    }
    
    @IBAction func clearTapped(sender: UIButton) {
        
        imvPhotoID.image = UIImage()
    }
    
    // label tapped
    func tapFunction(sender: UITapGestureRecognizer) {
        
        infoView.hidden = false
        
        UIView.animateWithDuration(0.5, animations: { () -> Void in
            self.infoView.alpha = 1
            }, completion: { finished in
                
                self.performSelector(#selector(self.hideInfoView), withObject: nil, afterDelay: 1.5)
        })
    }
    
    func hideInfoView() {
        
        UIView.animateWithDuration(0.5, animations: { () -> Void in
            self.infoView.alpha = 0
            }, completion: { finished in
                self.infoView.hidden = finished
        })
    }
    
    
    /*
    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func drawRect(rect: CGRect) {
        // Drawing code
    }
    */

}

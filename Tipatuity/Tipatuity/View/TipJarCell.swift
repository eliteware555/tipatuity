//
//  TipJarCell.swift
//  Tipatuity
//
//  Created by Victory on 18/06/16.
//  Copyright © 2016 elite. All rights reserved.
//

import UIKit

class TipJarCell: UITableViewCell {
    
    @IBOutlet weak var brand: TipJarCustomView!
    
    @IBOutlet weak var lblBrandName: UILabel!
    @IBOutlet weak var lblDescription: UILabel!
    @IBOutlet weak var lblReceived: UILabel!
    @IBOutlet weak var lblMembers: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func setTipJar(item: TipJarModel) {
        
        brand.brandImage = UIImage(named: item.tipImage)!
        
        lblBrandName.text = item.tipTitle
        
        if (item.tipDescription.characters.count > 28) {
        
            lblDescription.text = "\"" + item.tipDescription.substringToIndex(item.tipDescription.endIndex.advancedBy(28 - item.tipDescription.characters.count)) + "...\""
            
        } else {
            
            lblDescription.text = "\"" + item.tipDescription + "\""
        }
        
        lblReceived.text = "$ " + "\(item.tipReceived)"
        
        lblMembers.text = "\(item.tipMembers)"
    }
}

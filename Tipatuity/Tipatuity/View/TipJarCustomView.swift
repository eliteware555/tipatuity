//
//  TipJarCustomView.swift
//  Tipatuity
//
//  Created by Victory on 18/06/16.
//  Copyright © 2016 elite. All rights reserved.
//

import UIKit

@IBDesignable class TipJarCustomView: UIView {
    
    private var contentView: UIView!
    
    @IBOutlet weak var imvBrand: NZCircularImageView!
    
    @IBOutlet weak var imvBadge: UIImageView!
    
    @IBInspectable var brandImage: UIImage = UIImage() {
        
        didSet {
            
            imvBrand.image = brandImage
        }
    }

    @IBInspectable var badgeImage: UIImage = UIImage() {
        
        didSet {
            
            imvBadge.image = badgeImage
        }
    }
    
    @IBInspectable var cornerRadius: CGFloat = 0 {
        
        didSet {
            
            imvBadge.layer.cornerRadius = cornerRadius
            imvBadge.layer.masksToBounds = true
        }
    }
    
    // for using TipJarCustomView in IB
    required init?(coder aDecoder: NSCoder) {
        
        super.init(coder: aDecoder)
        
        xibSetup()
    }
    
    // for using TipJarCustomView in Code
    override init(frame: CGRect)  {
        
        super.init(frame: frame)
        
        xibSetup()
    }
    
    private func loadFromNib() -> UIView {
        
        let bundle = NSBundle(forClass: self.dynamicType)
        let nib = UINib(nibName: "TipJarCustomView", bundle: bundle)
        let view = nib.instantiateWithOwner(self, options: nil)[0] as! UIView
        
        return view
    }
    
    private func xibSetup() {
        
        contentView = loadFromNib()
        contentView.frame = bounds
        contentView.autoresizingMask = [.FlexibleWidth, .FlexibleHeight]
        
        addSubview(contentView)
    }
    
    override func prepareForInterfaceBuilder() {
        
        imvBrand.image = brandImage
        imvBadge.image = badgeImage
        imvBadge.layer.cornerRadius = cornerRadius
        imvBadge.layer.masksToBounds = true
    }

    /*
    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func drawRect(rect: CGRect) {
        // Drawing code
    }
    */

}


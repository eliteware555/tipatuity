//
//  CustomUserview.m
//  Shehadi
//
//  Created by Victory on 30/05/16.
//  Copyright © 2016 victory. All rights reserved.
//

#import "CustomUserview.h"
@import JLToast;

@interface CustomUserView()

@end

@implementation CustomUserView

- (instancetype) initWithFrame:(CGRect)frame {
    
    
    self = [super initWithFrame:frame];
    
    if (self) {
        
        // initialize code here
        
        [self customInit];
    }
    
    return self;
}

- (instancetype) initWithCoder:(NSCoder *)aDecoder {
    
    self = [super initWithCoder:aDecoder];
    
    if (self) {
        
        [self customInit];
    }
    
    return self;
}

// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
    
    [self customInit];
}

- (void)setNeedsLayout {
    
    [super setNeedsLayout];
    [self setNeedsDisplay];
}

- (void)prepareForInterfaceBuilder {
    
    [self customInit];
}

- (void) customInit {
    
    self.imvUser.image = self.userImage;

    self.imvUser.borderColor = self.borderColor;
    self.imvUser.borderWidth = [NSNumber numberWithInteger:self.borderWidth];
    
    // badge Icon
    self.imvIcon.layer.cornerRadius = self.badgeViewCornerRadius;
    self.imvIcon.layer.borderWidth = self.badgeViewBorderWidth;
    self.imvIcon.layer.borderColor = self.borderColor.CGColor;

    self.imvIcon.layer.masksToBounds = YES;
    
    // set back background color
    [self setBadgeColor:self.badgeBackColor];
    
    self.imvIcon.image = self.badgeImage;
    self.imvIcon.image = [self.imvIcon.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    self.imvIcon.tintColor = [UIColor whiteColor];
    
    self.imvUser.userInteractionEnabled = true;
    
    UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleSingleTap:)];
    singleTap.numberOfTapsRequired = 1;
    [self addGestureRecognizer:singleTap];
}

- (void) handleSingleTap: (UITapGestureRecognizer *)recognizer {
    
    if (self.tag >= 500 && self.tag < 600) {
    
        [[JLToast makeText:[NSString stringWithFormat:@"incoming: user-%d pressed", (int)(self.tag- 500)] duration:1.5] show];
        
    } else if (self.tag >= 600) {
        
        [[JLToast makeText:[NSString stringWithFormat:@"outgoing: user-%d pressed", (int)(self.tag- 599)] duration:1.5] show];
        
    } else {
        
        [[JLToast makeText:@"user pressed" duration:1.5] show];
    }
}

- (void) setBadgeColor: (UIColor *) badgeColor {
    
    self.imvIcon.backgroundColor = badgeColor;
}

- (void) setHiddenBadge: (BOOL) badgeHidden {
    
    self.imvIcon.hidden = badgeHidden;
}


@end

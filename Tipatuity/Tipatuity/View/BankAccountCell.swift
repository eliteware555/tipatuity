//
//  BankAccountCell.swift
//  Tipatuity
//
//  Created by Victory on 21/06/16.
//  Copyright © 2016 elite. All rights reserved.
//

import UIKit

class BankAccountCell: UITableViewCell {
    
    @IBOutlet weak var lblBankName: UILabel!
    @IBOutlet weak var lblStatus: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

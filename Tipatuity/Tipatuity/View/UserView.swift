//
//  UserView.swift
//  Tipatuity
//
//  Created by Victory on 22/06/16.
//  Copyright © 2016 elite. All rights reserved.
//

import UIKit

protocol UserViewDelegate: class {
    
    func userTapped(sender: UserView)
}

@IBDesignable class UserView: UIView {
    
    private var contentView: UIView!
    
    @IBOutlet weak var userImageView: CircularImageView!
    @IBOutlet weak var badgeImageView: CircularImageView!
    
    weak var delegate: UserViewDelegate?
    
    @IBInspectable var userImage: UIImage = UIImage() {
        
        didSet {
            
            userImageView.image = userImage
        }
    }
    
    @IBInspectable var cornerRadius: CGFloat = 0 {
        
        didSet {
            
            userImageView.cornerRadius = cornerRadius
            
//            print("cornerRadius of UserView: ------- " + "\(cornerRadius)")
        }
    }
    
    @IBInspectable var borderWidth: CGFloat = 0 {
        
        didSet {
            
            userImageView.borderWidth = borderWidth
        }
    }
    
    @IBInspectable var borderColor: UIColor = UIColor.whiteColor() {
        
        didSet {
            
            userImageView.borderColor = borderColor
        }
    }
    
    @IBInspectable var badgeImage: UIImage = UIImage() {
        
        didSet {

            badgeImageView.image = badgeImage
            badgeImageView.image = badgeImageView.image?.imageWithRenderingMode(.AlwaysTemplate)
            badgeImageView.tintColor = UIColor.whiteColor()
        }
    }
    
    @IBInspectable var badgeCornerRadius: CGFloat = 0 {
        
        didSet {
            
            badgeImageView.cornerRadius = badgeCornerRadius
        }
    }
    
    @IBInspectable var badgeBorderWidth: CGFloat = 0 {
        
        didSet {
            
            badgeImageView.borderWidth = badgeBorderWidth
        }
    }
    
    @IBInspectable var badgeBorderColor: UIColor = UIColor.whiteColor() {
        
        didSet {
            
            badgeImageView.borderColor = badgeBorderColor
        }
    }
    
    @IBInspectable var badgeBackgroundColor: UIColor = UIColor.whiteColor() {
        
        didSet {
            
            badgeImageView.backgroundColor = badgeBackgroundColor
        }
    }
    
    @IBInspectable var isVisisbleBadgeView: Bool = true {
        
        didSet {
            
            badgeImageView.hidden = !isVisisbleBadgeView
        }
    }
    
    // for using in IB
    required init?(coder aDecoder: NSCoder) {
        
        super.init(coder: aDecoder)
        
        xibSetup()
        
//        print(bounds.size.height, bounds.size.width)
        
        commonInit()
    }
    
    // for using code
    override init(frame: CGRect) {
        
        super.init(frame: frame)
        
        xibSetup()
        
        commonInit()
        
//        print(bounds.size.height, bounds.size.width)
    }
    
    private func loadFromNib() -> UIView {
        
        let bundle = NSBundle(forClass: self.dynamicType)
        let nib = UINib(nibName: "UserView", bundle: bundle)
        let view = nib.instantiateWithOwner(self, options: nil)[0] as! UIView
        
        return view
    }
    
    private func xibSetup() {
        
        contentView = loadFromNib()
        
        // use bounds not frame or it'll be offset
        contentView.frame = bounds
        
        contentView.autoresizingMask = [.FlexibleWidth, .FlexibleHeight]
        
        addSubview(contentView)
    }
    
    func commonInit() {
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(tapFunction))
        userImageView.addGestureRecognizer(tap)
        userImageView.userInteractionEnabled = true
    }
    
    func tapFunction() {
        
        delegate?.userTapped(self)
    }
   
    override func prepareForInterfaceBuilder() {
        
        super.prepareForInterfaceBuilder()
        
        userImageView.image = userImage
        userImageView.cornerRadius = cornerRadius
        userImageView.borderWidth = borderWidth
        userImageView.borderColor = borderColor
        
        badgeImageView.image = badgeImage
        badgeImageView.cornerRadius = badgeCornerRadius
        badgeImageView.borderWidth = badgeBorderWidth
        badgeImageView.borderColor = badgeBorderColor
        
        badgeImageView.backgroundColor = badgeBackgroundColor
        
        badgeImageView.hidden = !isVisisbleBadgeView
    }
    
    func setBadge(image: UIImage) {
        
        badgeImageView.image = image
        badgeImageView.image = badgeImageView.image?.imageWithRenderingMode(.AlwaysTemplate)
        badgeImageView.tintColor = UIColor.whiteColor()
    }
    
    func setBadgeColor(color: UIColor) {
        
        badgeImageView.backgroundColor = color
    }
    
    /*
    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func drawRect(rect: CGRect) {
        // Drawing code
    }
    */

}

//
//  ReviewPersonalInfoView.swift
//  Tipatuity
//
//  Created by Victory on 19/06/16.
//  Copyright © 2016 elite. All rights reserved.
//

import UIKit
import FontAwesome

protocol ReviewPersonalInfoDelegate: class {
    
    func retryButtonClicked(sender: ReviewPersonalInfoView)
}

@IBDesignable class ReviewPersonalInfoView: UIView {
    
    private var contentView: UIView!
    
    @IBOutlet weak var lblDescription: UILabel!
    
    @IBOutlet weak var infoView: UIView!
    
    weak var delegate: ReviewPersonalInfoDelegate?
    
    // for use in IB
    required init?(coder aDecoder: NSCoder) {
        
        super.init(coder: aDecoder)
        
        xibSetup()
        
        commonInit()
    }
    
    // for use in Code
    override init(frame: CGRect) {
        
        super.init(frame: frame)
        // extra function go in after super.init
        
        xibSetup()
        
        commonInit()
    }

    private func loadFromNib() -> UIView {
        
        let bundle = NSBundle(forClass: self.dynamicType)
        let nib = UINib(nibName: "ReviewPersonalInfoView", bundle: bundle)
        let view = nib.instantiateWithOwner(self, options: nil)[0] as! UIView
        
        return view
    }
    
    private func xibSetup() {
        
        contentView = loadFromNib()
        contentView.frame = bounds
        contentView.autoresizingMask = [.FlexibleWidth, .FlexibleHeight]
        
        addSubview(contentView)
    }
    
    func commonInit() {
        
        // add blur effect view
        let blurEffect = UIBlurEffect(style: .Dark)
        let blurEffectView = UIVisualEffectView(effect: blurEffect)
        blurEffectView.frame = self.bounds
        
        blurEffectView.autoresizingMask = [.FlexibleWidth, .FlexibleHeight]
        insertSubview(blurEffectView, atIndex: 0)
        
        // add info mark at the last of label text (fontawesome)
        
        let blueAttribute = [NSForegroundColorAttributeName: UIColor(hex: 0x00BBFF)]
        
        lblDescription.font = UIFont.fontAwesomeOfSize(14)
   
        let strDescription = "We were unable to verify your identify, please review your personal information and help us activate your account "

        let attributedText = NSMutableAttributedString(string: strDescription + String.fontAwesomeIconWithCode("fa-exclamation-circle")!)
        attributedText.addAttributes(blueAttribute, range: NSMakeRange(strDescription.characters.count, 1))
        
        lblDescription.attributedText = attributedText
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(tapFunction(_:)))
        lblDescription.addGestureRecognizer(tap)
        
        lblDescription.userInteractionEnabled = true
        
        infoView.hidden = true
        infoView.alpha = 0
        
    }
    
    override func prepareForInterfaceBuilder() {
        
        super.prepareForInterfaceBuilder()
    }
    
    func showView(onView: UIView) {
        
        onView.addSubview(self)
    }
    
    func hideView() {
        
        removeFromSuperview()
    }
    
    // label tapped
    func tapFunction(sender: UITapGestureRecognizer) {
        
        infoView.hidden = false
        
        UIView.animateWithDuration(0.5, animations: { () -> Void in
            
            self.infoView.alpha = 1
            
            }, completion: { finished in
                
                self.performSelector(#selector(self.hideInfoView), withObject: nil, afterDelay: 1.5)
        })
    }
    
    func hideInfoView() {
        
        UIView.animateWithDuration(0.5, animations: { () -> Void in
            self.infoView.alpha = 0
            }, completion: { finished in
                self.infoView.hidden = finished
        })
    }
    
    @IBAction func retryTapped(sender: UIButton) {
        
        delegate?.retryButtonClicked(self)
    }
    
    /*
    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func drawRect(rect: CGRect) {
        // Drawing code
    }
    */

}

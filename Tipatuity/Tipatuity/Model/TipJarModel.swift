//
//  TipJarModel.swift
//  Tipatuity
//
//  Created by master on 6/18/16.
//  Copyright © 2016 elite. All rights reserved.
//

import UIKit

class TipJarModel: NSObject {
    
    var tipImage: String!
    var tipTitle: String!
    var tipDescription: String!
    var tipReceived: Float
    var tipMembers: Int
    
    var isActivie: Bool!
    
    override init() {
        
        tipImage = ""
        tipTitle = ""
        tipDescription = ""
        tipReceived = 0.0
        tipMembers = 0
        isActivie = true
    }
    
    init(tipImage: String, tipTitle: String, tipDescription: String, tipReceived: Float, tipMembers: Int, isActive: Bool) {
        
        self.tipImage = tipImage
        self.tipTitle = tipTitle
        self.tipDescription = tipDescription
        
        self.tipReceived = tipReceived
        self.tipMembers = tipMembers
        
        self.isActivie = isActive
    }
}





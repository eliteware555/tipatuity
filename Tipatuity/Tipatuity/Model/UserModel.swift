//
//  UserModel.swift
//  Tipatuity
//
//  Created by Victory on 22/06/16.
//  Copyright © 2016 elite. All rights reserved.
//

import UIKit

class UserModel: NSObject {
    
    var name: String!
    var email: String!
    var rating: Float
    
    var image: String!
    var badgeImage: String!
    var isFlag: Bool
    
    var distance: Float
    
    override init() {
        
        name = ""
        email = ""
        rating = 0.0
        
        image = ""
        badgeImage = ""
        isFlag = true
        
        distance = 0.0
    }
    
    init(name: String, email: String, rating: Float, image: String, badgeImage: String, distance: Float, isFlag: Bool) {
        
        self.name = name
        self.email = email
        self.rating = rating
        self.image = image
        self.badgeImage = badgeImage
        self.isFlag = isFlag
        self.distance = distance
    }
}

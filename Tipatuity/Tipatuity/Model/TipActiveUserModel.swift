//
//  ActiveTipUserModel.swift
//  Tipatuity
//
//  Created by Victory on 19/06/16.
//  Copyright © 2016 elite. All rights reserved.
//

import UIKit

class TipActiveUserModel: NSObject {

    var userName: String
    var userEmail: String
    var rate: Float
    var userImage: String
    var isOnline: Bool
    
    override init() {
        
        userName = ""
        userEmail = ""
        rate = 0.0
        userImage = ""
        isOnline = false
    }
    
    init(userName: String, userImage: String!, userEmail: String, rate: Float, isOnline: Bool) {
        
        self.userName = userName
        self.userImage = userImage
        self.userEmail = userEmail
        self.rate = rate
        self.isOnline = isOnline
    }
}

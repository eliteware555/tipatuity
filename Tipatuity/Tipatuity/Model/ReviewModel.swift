//
//  RatingModel.swift
//  Tipatuity
//
//  Created by Victory on 28/06/16.
//  Copyright © 2016 elite. All rights reserved.
//

import UIKit

class ReviewModel: NSObject {
    
    var user: UserModel!
    var location: String!
    var country: String!
    var rating: Float!
    var review: String!
    
    override init() {
        
        user = UserModel()
        location = ""
        country = ""
        rating = 0.0
        review = ""
    }
    
    init(user: UserModel, location: String, country: String, rating: Float, review: String) {
        
        self.user = user
        self.location = location
        self.country = country
        self.rating = rating
        self.review = review
    }
}

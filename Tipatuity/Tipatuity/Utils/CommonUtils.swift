//
//  CommonUtils.swift
//  Tipatuity
//
//  Created by Victory on 19/06/16.
//  Copyright © 2016 elite. All rights reserved.
//

import UIKit

class CommonUtils {
    
    class func isValideEmail(email: String) -> Bool {
        
        let emailRegEx = "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluateWithObject(email)
    }
    
    // save image to a file (Documents/Tipatuity/tipjar.png)
    class func saveToFile(image: UIImage!) -> String! {
        
        var savedPhotoURL = ""
        
        let fileManager = NSFileManager.defaultManager()
        let paths = NSSearchPathForDirectoriesInDomains(.DocumentDirectory, .UserDomainMask, true)
        
        var documentDirectory: NSString! = paths[0]
        
        // current document directory
        fileManager.changeCurrentDirectoryPath(documentDirectory as String)
        
        do {
            
            try fileManager.createDirectoryAtPath("Tipatuity", withIntermediateDirectories: true, attributes: nil)
            
        } catch let error as NSError {
            
            print(error.localizedDescription)
        }
        
        documentDirectory = documentDirectory.stringByAppendingPathComponent("Tipatuity")
        let filePath = documentDirectory.stringByAppendingPathComponent("tipjar")
        
        // if the file exists already, delete and write, else if create filePath
        
        if (fileManager.fileExistsAtPath(filePath)) {
            do {
                try fileManager.removeItemAtPath(filePath)
            }
            catch let error as NSError {
                print("Ooops! Something went wrong: \(error)")
            }
        } else {
            
            fileManager.createFileAtPath(filePath, contents: nil, attributes: nil)
        }
        
        if let data = UIImagePNGRepresentation(image) {
            
            data.writeToFile(filePath, atomically: true)
        }
        
        savedPhotoURL = filePath
        
        return savedPhotoURL
    }
    
    /**
     Returns a percent-escaped string following RFC 3986 for a query string key or value.
     RFC 3986 states that the following characters are "reserved" characters.
     - General Delimiters: ":", "#", "[", "]", "@", "?", "/"
     - Sub-Delimiters: "!", "$", "&", "'", "(", ")", "*", "+", ",", ";", "="
     In RFC 3986 - Section 3.4, it states that the "?" and "/" characters should not be escaped to allow
     query strings to include a URL. Therefore, all "reserved" characters with the exception of "?" and "/"
     should be percent-escaped in the query string.
     - parameter string: The string to be percent-escaped.
     - returns: The percent-escaped string.
     */
    class func escape(string: String) -> String {
        
        let generalDelimitersToEncode = ":#[]@" // does not include "?" or "/" due to RFC 3986 - Section 3.4
        let subDelimitersToEncode = "!$&'()*+,;="
        
        let allowedCharacterSet = NSCharacterSet.URLQueryAllowedCharacterSet().mutableCopy() as! NSMutableCharacterSet
        allowedCharacterSet.removeCharactersInString(generalDelimitersToEncode + subDelimitersToEncode)
        
        var escaped = ""
        
        //==========================================================================================================
        //
        //  Batching is required for escaping due to an internal bug in iOS 8.1 and 8.2. Encoding more than a few
        //  hundred Chinese characters causes various malloc error crashes. To avoid this issue until iOS 8 is no
        //  longer supported, batching MUST be used for encoding. This introduces roughly a 20% overhead. For more
        //  info, please refer to:
        //
        //      - https://github.com/Alamofire/Alamofire/issues/206
        //
        //==========================================================================================================
        
//        if #available(iOS 8.3, OSX 10.10, *) {
            escaped = string.stringByAddingPercentEncodingWithAllowedCharacters(allowedCharacterSet) ?? string
//        } else {
//            let batchSize = 50
//            var index = string.startIndex
//            
//            while index != string.endIndex {
//                let startIndex = index
//                let endIndex = index.advancedBy(batchSize, limit: string.endIndex)
//                let range = startIndex..<endIndex
//                
//                let substring = string.substringWithRange(range)
//                
//                escaped += substring.stringByAddingPercentEncodingWithAllowedCharacters(allowedCharacterSet) ?? substring
//                
//                index = endIndex
//            }
//        }
        
        return escaped
    }
    
    class func getStateList() -> [String] {
        
//        NSString *plistPath = [[NSBundle mainBundle] pathForResource:@"interests" ofType:@"plist"];
//        NSDictionary *dictInterests = [NSDictionary dictionaryWithContentsOfFile:plistPath];
//        
//        interests = [dictInterests objectForKey:@"interests"];
        
        var dictStates: NSDictionary? = nil
        
        if let plistPath = NSBundle.mainBundle().pathForResource("StateList", ofType: "plist") {
        
            dictStates = NSDictionary(contentsOfFile: plistPath)
        }
        
        if let _ = dictStates {
            
            return dictStates!.objectForKey("State") as! [String]
            
        } else {
            
            return []
        }
    }
    
    
}

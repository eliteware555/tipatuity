//
//  Tipatuity-Bridging-Header.h
//  Tipatuity
//
//  Created by Victory on 14/06/16.
//  Copyright © 2016 elite. All rights reserved.
//

#ifndef Tipatuity_Bridging_Header_h
#define Tipatuity_Bridging_Header_h

#import "BFPaperTabBarController.h"
#import "M13BadgeView.h"
#import "NZCircularImageView.h"
#import "CarbonKit.h"
#import "M13Checkbox.h"
#import "UIImage+ResizeMagick.h"
#import "BKPasscodeField.h"
#import "BKPasscodeInputView.h"

#endif /* Tipatuity_Bridging_Header_h */
